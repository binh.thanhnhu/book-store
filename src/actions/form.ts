import { Dispatch } from "redux";
import delay from "utils/delay";
import { actionTypes } from "../constants/actionTypes";
import { IBillingDetails } from "../constants/interfaces";
import callApi from "../utils/apiCaller";

const setBillingDetails = (
  dispatch: Dispatch,
  detail: IBillingDetails,
): void => {
  dispatch({
    type: actionTypes.SET_BILLING_DETAILS,
    payload: detail,
  });
};

const sendBillingDetails = (
  dispatch: Dispatch,
  detail: IBillingDetails,
): void => {
  callApi("POST", "api/user/detail", detail)
    .then(async (res) => {
      await delay(dispatch, actionTypes.STOP_LOADING);
      dispatch({
        type: actionTypes.UPDATE_BILLING_DETAIL_SUCCESS,
        payload: res?.data,
      });
    })
    .catch(async (error) => {
      await delay(dispatch, actionTypes.STOP_LOADING);
      if (error.response) {
        dispatch({
          type: actionTypes.UPDATE_BILLING_DETAIL_FAILURE,
          payload: error.response.data,
        });
      }
      throw error;
    });
};

export { setBillingDetails, sendBillingDetails };

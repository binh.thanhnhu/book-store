import { IBillingDetails, IOder } from "constants/interfaces";
import { Dispatch } from "redux";
import callApi from "utils/apiCaller";
import delay from "utils/delay";
import { actionTypes } from "../constants/actionTypes";

const checkout = async (
  dispatch: Dispatch,
  details: IBillingDetails,
  products: IOder,
  token: string,
): Promise<boolean> => {
  await delay(dispatch, actionTypes.LOADING);
  console.log({ products });
  try {
    const res = await callApi(
      "POST",
      "api/order/checkout",
      {
        details,
        products,
      },
      token && token,
    );
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.CHECKOUT_SUCCESS,
      payload: res?.data,
    });
    return true;
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.CHECKOUT_FAILURE,
        payload: error.response.data,
      });
    }
    return false;
  }
};

export { checkout };

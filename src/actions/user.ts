import { IUserInfo } from "constants/interfaces";
import { Dispatch } from "redux";
import delay from "utils/delay";
import { actionTypes } from "../constants/actionTypes";
import callApi from "../utils/apiCaller";

interface IAuth {
  username: string;
  password: string;
}

const openLoginForm = (dispatch: Dispatch): void => {
  dispatch({
    type: actionTypes.OPEN_LOGIN_FORM,
  });
};

const closeLoginForm = (dispatch: Dispatch): void => {
  dispatch({
    type: actionTypes.CLOSE_LOGIN_FORM,
  });
};

const login = async (dispatch: Dispatch, values: IAuth): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);
  try {
    const res = await callApi("POST", "api/customer/login", {
      username: values.username,
      password: values.password,
    });
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.LOGIN_SUCCESS,
      payload: res?.data,
    });
    closeLoginForm(dispatch);
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.LOGIN_FAILURE,
        payload: error.response.data,
      });
    } else {
      throw error;
    }
  }
};

const register = async (dispatch: Dispatch, values: IAuth): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);
  try {
    const res = await callApi("POST", "api/customer/register", {
      username: values.username,
      password: values.password,
    });
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.REGISTER_SUCCESS,
      payload: res?.data,
    });
    closeLoginForm(dispatch);
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.REGISTER_FAILURE,
        payload: error.response.data,
      });
    } else {
      throw error;
    }
  }
};

const logout = async (dispatch: Dispatch): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);
  try {
    const res = await callApi("POST", "api/customer/logout", null);
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.LOGOUT_SUCCESS,
      payload: res?.data,
    });
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.LOGOUT_FAILURE,
        payload: error.response.data,
      });
    } else {
      throw error;
    }
  }
};

const resetMessageLoginForm = (dispatch: Dispatch): void => {
  dispatch({
    type: actionTypes.RESET_MESSAGE_LOGIN_FORM,
  });
};

const fetchOrdersHistory = async (
  dispatch: Dispatch,
  token: string,
): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);
  try {
    const res = await callApi("GET", "api/order/order-history", null, token);
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.GET_ORDER_HISTORY_SUCCESS,
      payload: res?.data,
    });
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.GET_ORDER_HISTORY_FAILURE,
        payload: error.response.data,
      });
    } else {
      throw error;
    }
  }
};

const fetchUserInfo = async (
  dispatch: Dispatch,
  token: string,
  updateToSession: boolean,
): Promise<boolean> => {
  await delay(dispatch, actionTypes.LOADING);
  try {
    const res = await callApi("GET", "api/customer/get-info", null, token);
    await delay(dispatch, actionTypes.STOP_LOADING);

    if (updateToSession) {
      const temp: any = res?.data.data;
      delete temp._id;
      sessionStorage.setItem("user_info", JSON.stringify(temp));
    }

    dispatch({
      type: actionTypes.GET_USER_INFO_SUCCESS,
      payload: res?.data,
    });

    return true;
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.GET_USER_INFO_FAILURE,
        payload: error.response.data,
      });

      return false;
    } else {
      throw error;
    }
  }
};

const updateUserInfo = async (
  dispatch: Dispatch,
  values: IUserInfo,
  token: string,
): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);
  try {
    const res = await callApi("PUT", "api/customer/update", values, token);
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.UPDATE_USER_INFO_SUCCESS,
      payload: res?.data,
    });
    console.log(1111);
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.UPDATE_USER_INFO_FAILURE,
        payload: error.response.data,
      });
    } else {
      throw error;
    }
  }
};

export {
  openLoginForm,
  closeLoginForm,
  login,
  logout,
  register,
  resetMessageLoginForm,
  fetchOrdersHistory,
  updateUserInfo,
  fetchUserInfo,
};

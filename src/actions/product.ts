import { Dispatch } from "redux";
import delay from "utils/delay";
import { actionTypes } from "../constants/actionTypes";
import { ICategories, IParam, IProduct } from "../constants/interfaces";
import addParams from "../utils/addParams";
import callApi from "../utils/apiCaller";

const fetchCategory = async (
  dispatch: Dispatch,
  searchUrl?: string | null,
  params?: IParam,
): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);

  const url = "api/product/option?" + addParams(searchUrl || "", params);

  try {
    const res = await callApi("GET", url);
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.GET_PRODUCT_OPTION_SUCCESS,
      payload: res?.data,
    });
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.GET_PRODUCT_OPTION_FAILURE,
        payload: error.response.data,
      });
    } else {
      throw error;
    }
  }
};

const fetchCategories = async (
  dispatch: Dispatch,
  categories: ICategories[],
): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);
  const callMultiApi = categories.map((category) =>
    callApi("GET", `api/product/category?category=${category}`),
  );

  try {
    const values = await Promise.all(callMultiApi);
    console.log(values);
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.GET_PRODUCT_CATEGORIES_SUCCESS,
      payload: categories.map((category, index) => {
        console.log({ category, d: values[index]?.data });
        return { categoryName: category, data: values[index]?.data };
      }),
    });
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.GET_PRODUCT_CATEGORIES_FAILURE,
        payload: error.response.data,
      });
    } else {
      throw error;
    }
  }
};

const getProductById = async (
  dispatch: Dispatch,
  id: string,
): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);

  try {
    const res = await callApi("GET", `api/product/get-by-id/${id}`);
    await delay(dispatch, actionTypes.STOP_LOADING);
    dispatch({
      type: actionTypes.GET_PRODUCT_BY_ID_SUCCESS,
      payload: res?.data,
    });
  } catch (error) {
    await delay(dispatch, actionTypes.STOP_LOADING);
    if (error.response) {
      dispatch({
        type: actionTypes.GET_PRODUCT_BY_ID_FAILURE,
        payload: error.response.data,
      });
    } else {
      throw error;
    }
  }
};

const openQuickview = (
  dispatch: Dispatch,
  open: boolean,
  product: IProduct | null,
): void => {
  dispatch({
    type: actionTypes.OPEN_QUICKVIEW,
    payload: { open, product },
  });
};

export { fetchCategory, fetchCategories, getProductById, openQuickview };

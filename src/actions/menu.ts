import { Dispatch } from "redux";
import { actionTypes } from "../constants/actionTypes";

const openMenu = (dispatch: Dispatch, open: boolean): void => {
  dispatch({ type: actionTypes.OPEN_MENU, payload: open });
};

const openMenuMobile = (dispatch: Dispatch, open: boolean): void => {
  dispatch({ type: actionTypes.OPEN_MENU_MOBILE, payload: open });
};

export { openMenu, openMenuMobile };

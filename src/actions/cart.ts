import { Dispatch } from "redux";
import delay from "utils/delay";
import { actionTypes } from "../constants/actionTypes";
import { IProduct, IProductInCart } from "../constants/interfaces";

const addToCart = async (
  dispatch: Dispatch,
  product: IProduct,
  quantity: number,
): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);
  await delay(dispatch, actionTypes.STOP_LOADING, 500);
  dispatch({
    type: actionTypes.ADD_TO_CART,
    payload: { product, quantity },
  });
};

const removeProductInCart = (dispatch: Dispatch, productId: string): void => {
  dispatch({
    type: actionTypes.REMOVE_PRODUCT_IN_CART,
    payload: productId,
  });
};

const updateCart = async (
  dispatch: Dispatch,
  productList: IProductInCart[],
  totalPrice: number,
): Promise<void> => {
  await delay(dispatch, actionTypes.LOADING);
  await delay(dispatch, actionTypes.STOP_LOADING, 500);
  dispatch({
    type: actionTypes.UPDATE_CART,
    payload: { productList, totalPrice },
  });
};

const setTotalPrice = (dispatch: Dispatch, total: number): void => {
  dispatch({
    type: actionTypes.SET_TOTAL_PRICE,
    payload: total,
  });
};

const setTotalPriceTemp = (dispatch: Dispatch, total: number): void => {
  dispatch({
    type: actionTypes.SET_TOTAL_PRICE_TEMP,
    payload: total,
  });
};

export {
  addToCart,
  removeProductInCart,
  updateCart,
  setTotalPrice,
  setTotalPriceTemp,
};

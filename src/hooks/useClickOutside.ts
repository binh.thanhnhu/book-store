import { useEffect } from "react";

export default function useClickOutside(
  // eslint-disable-next-line
  ref: any,
  condition?: boolean,
  // eslint-disable-next-line
  callback?: any
): void {
  useEffect(() => {
    if (condition) {
      const handleClickOutside = (event: MouseEvent | TouchEvent) => {
        if (ref.current && !ref.current.contains(event.target)) {
          callback();
        }
      };
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  }, [ref]);
}

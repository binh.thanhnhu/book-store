import * as Yup from "yup";

export const VBillingDetails = Yup.object({
  firstName: Yup.string().required("Vui lòng nhập tên của bạn"),
  lastName: Yup.string().required("Vui lòng nhập họ của bạn"),
  company: Yup.string(),
  country: Yup.string().required("Vui lòng chọn tên nước"),
  address: Yup.string().required("Vui lòng nhập địa chỉ"),
  postcode: Yup.string(),
  phone: Yup.string().required("Vui lòng nhập số điện thoại"),
  email: Yup.string()
    .email("Email không hợp lệ")
    .required("Vui lòng nhập email"),
  orderComments: Yup.string(),
});

export const VUserInfo = Yup.object({
  firstName: Yup.string().required("Vui lòng nhập tên của bạn"),
  lastName: Yup.string().required("Vui lòng nhập họ của bạn"),
  company: Yup.string(),
  country: Yup.string().required("Vui lòng chọn tên nước"),
  address: Yup.string().required("Vui lòng nhập địa chỉ"),
  postcode: Yup.string(),
  phone: Yup.string().required("Vui lòng nhập số điện thoại"),
  email: Yup.string()
    .email("Email không hợp lệ")
    .required("Vui lòng nhập email"),
});

export const VAccountLogin = Yup.object({
  username: Yup.string().required("Vui lòng nhập tên tài khoản"),
  password: Yup.string().required("Vui lòng nhập mật khẩu"),
});

export const VAccountRegister = Yup.object({
  username: Yup.string().required("Vui lòng nhập tên tài khoản"),
  password: Yup.string().required("Vui lòng nhập mật khẩu"),
  rePassword: Yup.string()
    .required("Vui lòng nhập lại mật khẩu")
    .oneOf([Yup.ref("password"), null], "Mật khẩu không khớp"),
});

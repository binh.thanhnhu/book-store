export const DProduct = {
  id: "",
  _id: "",
  type: "",
  option: "",
  img: "",
  title: "",
  author: "",
  price: 0,
  oldPrice: 0,
  rated: 0,
  description: "",
  description2: "",
  sku: "",
  customerReviews: 0,
  amount: 0,
  publishingCompany: "",
  numberOfPages: 0,
  size: "",
  dateOfPublication: undefined,
  tag: [],
};

export const DBillingDetails = {
  firstName: "",
  lastName: "",
  company: "",
  country: "",
  address: "",
  postcode: "",
  phone: "",
  email: "",
  orderComments: "",
};

export const DUserInfo = {
  firstName: "",
  lastName: "",
  company: "",
  country: "",
  address: "",
  postcode: "",
  phone: "",
  email: "",
};

export const DAccount = {
  username: "",
  password: "",
};

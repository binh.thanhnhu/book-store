export interface IProduct {
  _id: string;
  type?: string;
  option?: string;
  imageFromUrl: { public_id: string | ""; url: string | "" }[];
  title: string;
  author?: string;
  price: number;
  oldPrice?: number;
  rated?: number;
  description?: string;
  description2?: string;
  SKU?: string;
  customerReviews?: number;
  amount?: number;
  publishingCompany?: string;
  numberOfPages?: number;
  size?: string;
  dateOfPublication?: Date;
  tag?: string[];
}

export interface IMenu {
  id: string;
  href: string;
  name: string;
  option: string;
}

export interface IType {
  id: string;
  name: string;
  type: string;
  menu?: IMenu[];
}

export interface IResponse {
  status: number;
  success: boolean;
  data?: any;
}

export type ICategories =
  | "BestSeller"
  | "Newest"
  | "HighlightMonth"
  | "Discount";

export interface IParam {
  type?: string;
  option?: string;
  page?: number;
  layout?: string;
  sortName?: string;
  sort?: string;
  keyword?: string;
}

export interface IProductInCart {
  product: IProduct;
  quantity: number;
}

export interface IBillingDetails {
  firstName: string;
  lastName: string;
  company: string;
  country: string;
  address: string;
  postcode: string;
  phone: string;
  email: string;
  orderComments: string;
}

export interface IUserInfo {
  firstName: string;
  lastName: string;
  company: string;
  country: string;
  address: string;
  postcode: string;
  phone: string;
  email: string;
}

export interface IOderProduct {
  productId: string;
  productTitle: string;
  price: number;
  quantity: number;
}

export interface IOder {
  time: Date;
  info: IOderProduct[];
  totalPrice: number;
}

export interface IAccount {
  username: string;
  password: string;
  rePassword?: string;
}

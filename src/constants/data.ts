import { IType } from "./interfaces";

export const BookType: IType[] = [
  {
    id: "menu-item-001",
    name: "Sách Tiếng Việt",
    type: "VietNameseBook",
    menu: [
      {
        id: "menu-item-01",
        href: "#",
        name: "Sách văn học",
        option: "Literature",
      },
      {
        id: "menu-item-02",
        href: "#",
        name: "Sách kinh tế",
        option: "Economic",
      },
      {
        id: "menu-item-03",
        href: "#",
        name: "Sách thiếu nhi",
        option: "ChildrensBooks",
      },
      {
        id: "menu-item-04",
        href: "#",
        name: "Sách kỹ năng sống",
        option: "LifeSkill",
      },
      {
        id: "menu-item-05",
        href: "#",
        name: "Sách giáo khoa",
        option: "Textbook",
      },
      { id: "menu-item-06", href: "#", name: "Truyện tranh", option: "Comic" },
      {
        id: "menu-item-07",
        href: "#",
        name: "Sách tôn giáo / tâm linh",
        option: "ReligionAndSpirituality",
      },
      { id: "menu-item-08", href: "#", name: "Tạp chí", option: "Journal" },
    ],
  },
  {
    id: "menu-item-002",
    name: "Sách Nước Ngoài",
    type: "ForeignBook",
    menu: [
      {
        id: "menu-item-01",
        href: "#",
        name: "Cookbooks, Food & Wine",
        option: "Cookbooks",
      },
      {
        id: "menu-item-02",
        href: "#",
        name: "Science & Technology",
        option: "ScienceAndTechnology",
      },
      {
        id: "menu-item-03",
        href: "#",
        name: "Parenting & Relationships",
        option: "ParentingAndRelationships",
      },
      { id: "menu-item-04", href: "#", name: "Magazines", option: "Magazines" },
      {
        id: "menu-item-05",
        href: "#",
        name: "Dictionary",
        option: "Dictionary",
      },
      {
        id: "menu-item-06",
        href: "#",
        name: "Self Help",
        option: "SelfHelp",
      },
      {
        id: "menu-item-07",
        href: "#",
        name: "Art & Photography",
        option: "ArtAndPhotography",
      },
      {
        id: "menu-item-08",
        href: "#",
        name: "Children's Books",
        option: "ChildrensBooks",
      },
    ],
  },
  {
    id: "menu-item-003",
    name: "Văn phòng phẩm",
    type: "Stationery",
  },
  {
    id: "menu-item-004",
    name: "Quà lưu niệm",
    type: "Souvenir",
  },
  {
    id: "menu-item-005",
    name: "Ebook",
    type: "Ebook",
  },
];

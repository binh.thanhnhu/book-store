export const actionTypes = {
  OPEN_LOGIN_FORM: "OPEN_LOGIN_FORM",
  CLOSE_LOGIN_FORM: "CLOSE_LOGIN_FORM",
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGIN_FAILURE: "LOGIN_FAILURE",
  LOGOUT_SUCCESS: "LOGOUT_SUCCESS",
  LOGOUT_FAILURE: "LOGOUT_FAILURE",
  REGISTER_SUCCESS: "REGISTER_SUCCESS",
  REGISTER_FAILURE: "REGISTER_FAILURE",
  UPDATE_USER_INFO_SUCCESS: "UPDATE_USER_INFO_SUCCESS",
  UPDATE_USER_INFO_FAILURE: "UPDATE_USER_INFO_FAILURE",
  GET_USER_INFO_SUCCESS: "GET_USER_INFO_SUCCESS",
  GET_USER_INFO_FAILURE: "GET_USER_INFO_FAILURE",

  GET_PRODUCT_OPTION_SUCCESS: "GET_PRODUCT_OPTION_SUCCESS",
  GET_PRODUCT_OPTION_FAILURE: "GET_PRODUCT_OPTION_FAILURE",
  GET_PRODUCT_BY_ID_SUCCESS: "GET_PRODUCT_BY_ID_SUCCESS",
  GET_PRODUCT_BY_ID_FAILURE: "GET_PRODUCT_BY_ID_FAILURE",
  GET_QUICKVIEW_PRODUCT_BY_ID_SUCCESS: "GET_QUICKVIEW_PRODUCT_BY_ID_SUCCESS",
  GET_QUICKVIEW_PRODUCT_BY_ID_FAILURE: "GET_QUICKVIEW_PRODUCT_BY_ID_FAILURE",
  GET_PRODUCT_CATEGORIES_SUCCESS: "GET_PRODUCT_CATEGORIES_SUCCESS",
  GET_PRODUCT_CATEGORIES_FAILURE: "GET_PRODUCT_CATEGORIES_FAILURE",
  OPEN_QUICKVIEW: "OPEN_QUICKVIEW",

  ADD_TO_CART: "ADD_TO_CART",
  REMOVE_PRODUCT_IN_CART: "REMOVE_PRODUCT_IN_CART",
  UPDATE_CART: "UPDATE_CART",
  SET_TOTAL_PRICE: "SET_TOTAL_PRICE",
  SET_TOTAL_PRICE_TEMP: "SET_TOTAL_PRICE_TEMP",

  OPEN_MENU: "OPEN_MENU",
  OPEN_MENU_MOBILE: "OPEN_MENU_MOBILE",
  OVERRIDE_MENU_MOBILE: "OVERRIDE_MENU_MOBILE",

  SET_BILLING_DETAILS: "SET_BILLING_DETAILS",
  UPDATE_BILLING_DETAIL_SUCCESS: "UPDATE_BILLING_DETAIL_SUCCESS",
  UPDATE_BILLING_DETAIL_FAILURE: "UPDATE_BILLING_DETAIL_FAILURE",
  VALIDATE_BILLING_DETAILS_FORM: "VALIDATE_BILLING_DETAILS_FORM",
  RESET_MESSAGE_LOGIN_FORM: "RESET_MESSAGE_LOGIN_FORM",

  CHECKOUT_SUCCESS: "CHECKOUT_SUCCESS",
  CHECKOUT_FAILURE: "CHECKOUT_FAILURE",
  GET_ORDER_HISTORY_SUCCESS: "GET_ORDER_HISTORY_SUCCESS",
  GET_ORDER_HISTORY_FAILURE: "GET_ORDER_HISTORY_FAILURE",

  LOADING: "LOADING",
  STOP_LOADING: "STOP_LOADING",
};

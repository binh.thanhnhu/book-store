import CheckoutPageAfter from "pages/CheckoutPageSuccess";
import OrdersHistoryPage from "pages/OrdersHistoryPage";
import SearchPage from "pages/SearchPage";
import UserInfoPage from "pages/UserInfoPage";
import React, { ReactElement, useEffect } from "react";
import { useSelector } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { RootState } from ".";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Loading from "./components/Loading/Loading";
import Quickview from "./components/Quickview/Quickview";
import CartPage from "./pages/CartPage";
import CheckoutPage from "./pages/CheckoutPage";
import Homepage from "./pages/Homepage";
import ProductCategoryPage from "./pages/ProductCategoryPage";
import ProductDetailPage from "./pages/ProductDetailPage";
import "./styles/custom.scss";
import "./styles/layout.scss";
import "./styles/root.scss";
import callApi from "./utils/apiCaller";

function App(): ReactElement {
  const loading = useSelector((state: RootState) => state.loading?.loading);
  const quickviewProduct = useSelector(
    (state: RootState) => state.product.quickviewProduct,
  );

  useEffect(() => {
    callApi("POST", "api/statistics/increaseSiteVisit", null);
  }, []);

  return (
    <div className="App">
      <div className="page-template page-template-templates page-template-composer page-template-templatescomposer-php page page-id-3907 kingcomposer kc-css-system woocommerce-demo-store woocommerce-js wpb-js-composer js-comp-ver-6.4.1 vc_responsive">
        <div className="site-content">
          <Router>
            {loading && <Loading />}
            <Header />
            {quickviewProduct && <Quickview product={quickviewProduct} />}
            <Switch>
              <Route exact path="/" component={Homepage} />
              <Route path="/product/:id" component={ProductDetailPage} />
              <Route path="/cart" component={CartPage} />
              <Route exact path="/checkout" component={CheckoutPage} />
              <Route path="/checkout/success" component={CheckoutPageAfter} />
              <Route path="/product-category" component={ProductCategoryPage} />
              <Route path="/orders-history" component={OrdersHistoryPage} />
              <Route path="/user-info" component={UserInfoPage} />
              <Route path="/search-result" component={SearchPage} />
            </Switch>
            <Footer />
          </Router>
        </div>
      </div>
    </div>
  );
}

export default App;

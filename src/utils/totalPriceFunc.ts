import { IProductInCart } from "constants/interfaces";

export default function totalPriceFunc(
  products: IProductInCart[],
): { total: number; n: number } {
  let total = 0;
  let n = 0;
  products.forEach((obj: IProductInCart) => {
    total += obj.product.price * obj.quantity;
    n += obj.quantity;
  });
  return { total, n };
}

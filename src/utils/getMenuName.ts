function getName(
  // eslint-disable-next-line
  someArray: any,
  keyInput: string,
  keyOutput: string,
  string: string,
): any {
  let obj: any = {};
  for (let i = 0; i < someArray.length; i++) {
    if (someArray[i][keyInput] === string) {
      obj = someArray[i];
      break;
    }
  }
  return { name: obj[keyOutput], obj };
}

export default function getMenuName(
  // eslint-disable-next-line
  tree: any,
  type: string,
  option: string | undefined,
): { typeName: string; optionName?: string } {
  const { name: typeName, obj } = getName(tree, "type", "name", type);
  if (option) {
    const { name: optionName } = getName(obj.menu, "option", "name", option);
    return { typeName, optionName };
  } else {
    return { typeName };
  }
}

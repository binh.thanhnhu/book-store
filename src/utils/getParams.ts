export default function getParams(searchUrl: string): any {
  const query: any = {};
  const urlParams = new URLSearchParams(searchUrl);
  urlParams.forEach((value, key) => {
    query[key] = value;
  });
  return query;
}

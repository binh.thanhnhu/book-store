import { IParam } from "../constants/interfaces";

const addParams = (
  searchUrl: string,
  rest?: IParam,
  except?: string[],
): string => {
  const urlParams = new URLSearchParams(searchUrl);
  let query: any = {};
  let url = "";

  urlParams.forEach((value, key) => {
    if (!except?.includes(key)) query[key] = value;
  });
  query = { ...query, ...rest };
  Object.keys(query).map((key) => {
    if (query[key]) url += `${key}=${query[key]}&`;
  });

  return url;
};

export default addParams;

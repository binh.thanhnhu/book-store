import axios, { AxiosResponse, Method } from "axios";
const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";

export default function callApi(
  method: Method,
  endpoint: string,
  // eslint-disable-next-line
  data?: any,
  token?: string,
): Promise<AxiosResponse<any> | undefined> {
  if (token) {
    axios.defaults.headers.common = { Authorization: `Bearer ${token}` };
  }
  return axios({
    method,
    url: `${API_URL}/${endpoint}`,
    data,
  });
}

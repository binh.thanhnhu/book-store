export default function lowercase(string: string): string {
  return string ? string.charAt(0).toLowerCase() + string.slice(1) : string;
}

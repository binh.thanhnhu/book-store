import { Dispatch } from "redux";

export default function delay(
  dispatch: Dispatch,
  type: string,
  time?: number,
): Promise<any> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(dispatch({ type }));
    }, time || 0);
  });
}

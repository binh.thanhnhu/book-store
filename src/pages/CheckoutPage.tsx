import { checkout } from "actions/checkout";
import { fetchUserInfo } from "actions/user";
import Input from "components/Input/Input";
import Message from "components/Message/Message";
import PageHeader from "components/PageHeader/PageHeader";
import UserInfo from "components/UserInfo/UserInfo";
import YourOrder from "components/YourOrder/YourOrder";
import { DBillingDetails } from "constants/init";
import { IBillingDetails, IOder } from "constants/interfaces";
import { VBillingDetails } from "constants/validation";
import { Form, Formik } from "formik";
import { RootState } from "index";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router";

const CheckoutPage: React.FC = () => {
  const [open, setOpen] = useState<boolean>(false);
  const [DBillingDetails2, setDBillingDetails2] = useState<IBillingDetails>(
    JSON.parse(sessionStorage.getItem("user_info") || "{}"),
  );

  const location = useLocation();

  const productInCart = useSelector(
    (state: RootState) => state.cart.productInCart,
  );
  const totalPrice = useSelector((state: RootState) => state.cart.totalPrice);
  const message = useSelector(
    (state: RootState) => state.checkout.messageCheckout,
  );
  const token = useSelector((state: RootState) => state.user.token);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  useEffect(() => {
    const getUserInfo = async () => {
      if (
        token &&
        Object.keys(JSON.parse(sessionStorage.getItem("user_info") || "{}"))
          .length === 0
      ) {
        const result = await fetchUserInfo(dispatch, token, true);
        if (result) {
          setDBillingDetails2({
            ...DBillingDetails2,
            ...JSON.parse(sessionStorage.getItem("user_info") || "{}"),
          });
        }
      }
    };
    getUserInfo();
  }, [token]);

  const openCoupon = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    setOpen(!open);
  };

  const handleSubmit = async (values: IBillingDetails): Promise<void> => {
    console.log({ values, totalPrice });
    if (productInCart.length > 0) {
      const products: IOder = {
        time: new Date(),
        info: [],
        totalPrice: 0,
      };
      products.info = productInCart.map((o) => {
        return {
          productId: o.product._id,
          productTitle: o.product.title,
          price: o.product.price,
          quantity: o.quantity,
        };
      });
      products.totalPrice = totalPrice;
      sessionStorage.setItem("user_info", JSON.stringify(values));
      const success = await checkout(dispatch, values, products, token);
      if (success) history.push("/checkout/success");
    }
  };

  return (
    <>
      <PageHeader title="Checkout" />
      <main className="main-content">
        <div className="container">
          <div className="content-area">
            <div
              id="post-8"
              className="type-page post-8 page status-publish hentry"
            >
              <div className="woocommerce">
                <div className="woocommerce-notices-wrapper"></div>
                <div className="woocommerce-form-coupon-toggle">
                  <Message>
                    Có mã giảm giá?{" "}
                    <a href="#" className="showcoupon" onClick={openCoupon}>
                      {" "}
                      Click để nhập mã giảm giá
                    </a>{" "}
                  </Message>
                </div>
                <form
                  className="checkout_coupon woocommerce-form-coupon"
                  method="post"
                  style={open ? { display: "block" } : { display: "none" }}
                >
                  <p className="form-row form-row-first">
                    <input
                      type="text"
                      name="coupon_code"
                      className="input-text"
                      placeholder="Coupon code"
                      id="coupon_code"
                      defaultValue=""
                    />
                  </p>
                  <p className="form-row form-row-last">
                    <Input
                      tag="button"
                      type="submit"
                      className="button"
                      name="apply_coupon"
                      value="Apply coupon"
                    >
                      Apply coupon
                    </Input>
                  </p>
                  <div className="clear" />
                </form>

                <Formik
                  onSubmit={handleSubmit}
                  initialValues={DBillingDetails2 || DBillingDetails}
                  validationSchema={VBillingDetails}
                  enableReinitialize={true}
                >
                  {(formik) => (
                    <Form className="checkout woocommerce-checkout">
                      {console.log(formik.errors)}
                      {((Object.keys(formik.errors).length > 0 &&
                        Object.keys(formik.touched).length > 0) ||
                        (!formik.dirty && message)) && (
                        <Message error>
                          {Object.keys(formik.errors).length > 0 ? (
                            <ul className="woocommerce-error">
                              {Object.keys(formik.errors).map((key, i) => (
                                <li key={i}>
                                  {formik.errors[key as keyof IBillingDetails]}
                                </li>
                              ))}
                            </ul>
                          ) : (
                            !formik.dirty && message
                          )}
                        </Message>
                      )}
                      <UserInfo header="Thông tin khách hàng" />
                      <YourOrder />
                    </Form>
                  )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default CheckoutPage;

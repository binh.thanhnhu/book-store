import Message from "components/Message/Message";
import PageHeader from "components/PageHeader/PageHeader";
import UserInfo from "components/UserInfo/UserInfo";
import { Form, Formik } from "formik";
import { DUserInfo } from "constants/init";
import { IUserInfo } from "constants/interfaces";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "index";
import { VUserInfo } from "constants/validation";
import { updateUserInfo, fetchUserInfo } from "actions/user";
import { useHistory, useLocation } from "react-router";

const UserInfoPage: React.FC = () => {
  const messageFailure = useSelector(
    (state: RootState) => state.user.messageUpdateUserInfoFailure,
  );
  const messageSuccess = useSelector(
    (state: RootState) => state.user.messageUpdateUserInfoSuccess,
  );
  const [
    messageSuccessComp,
    setMessageSuccessComp,
  ] = useState<JSX.Element | null>(null);
  const [key, setKey] = useState<boolean>(false);

  const userInfo = useSelector((state: RootState) => state.user.userInfo);
  const token = useSelector((state: RootState) => state.user.token);
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  console.log(userInfo);

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  useEffect(() => {
    if (token && !userInfo) fetchUserInfo(dispatch, token, false);
    if (!token) history.push("/");
  }, [token]);

  useEffect(() => {
    let timer: any;
    if (messageSuccess) {
      setMessageSuccessComp(<Message>{messageSuccess}</Message>);
      timer = setTimeout(() => {
        setMessageSuccessComp(null);
      }, 5000);
    }
    return () => clearTimeout(timer);
  }, [key, messageSuccess]);

  const handleSubmit = async (
    values: IUserInfo,
    actions: any,
  ): Promise<void> => {
    updateUserInfo(dispatch, values, token);
    actions.resetForm({ values });
    setKey(!key);
  };

  return (
    <>
      <PageHeader title="Thông tin khách hàng" />
      <main className="main-content">
        <div className="container">
          <div className="content-area">
            <Formik
              onSubmit={handleSubmit}
              initialValues={userInfo || DUserInfo}
              validationSchema={VUserInfo}
              enableReinitialize
            >
              {(formik) => (
                <Form className="update-user-infomation">
                  {messageSuccess
                    ? messageSuccessComp
                    : ((Object.keys(formik.errors).length > 0 &&
                        Object.keys(formik.touched).length > 0) ||
                        (!formik.dirty && messageFailure)) && (
                        <Message error>
                          {Object.keys(formik.errors).length > 0 ? (
                            <ul className="woocommerce-error">
                              {Object.keys(formik.errors).map((key, i) => (
                                <li key={i}>
                                  {formik.touched[key as keyof IUserInfo] &&
                                    formik.errors[key as keyof IUserInfo]}
                                </li>
                              ))}
                            </ul>
                          ) : (
                            !formik.dirty && messageFailure
                          )}
                        </Message>
                      )}

                  <UserInfo split header="Thông tin cơ bản" />
                  <button
                    type="submit"
                    className="button alt"
                    id="update-user-info"
                    value="Cập nhật"
                  >
                    Cập nhật
                  </button>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </main>
    </>
  );
};

export default UserInfoPage;

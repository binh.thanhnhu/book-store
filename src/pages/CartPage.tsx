import { fetchCategories } from "actions/product";
import { RootState } from "index";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router";
import Cart from "../components/Cart/Cart";
import CartTotal from "../components/CartTotal/CartTotal";
import PageHeader from "../components/PageHeader/PageHeader";
import ProductGrid from "../components/ProductCategory/ProductGrid";

const CartPage: React.FC = () => {
  const location = useLocation();
  const dispatch = useDispatch();

  const listProductHighlightMonth = useSelector(
    (state: RootState) => state.product.highlightMonth,
  );

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  useEffect(() => {
    fetchCategories(dispatch, ["HighlightMonth"]);
  }, []);

  return (
    <>
      <PageHeader title="Cart" />
      <div className="main-content">
        <div className="container">
          <div className="content-area">
            <article
              id="post-7"
              className="type-page post-7 page status-publish hentry"
            >
              <div className="kc_clfw" />
              <div className="woocommerce">
                <div className="woocommerce-notices-wrapper" />
                <Cart />
                <CartTotal />
              </div>
            </article>
            <ProductGrid
              id="432932"
              name="Nổi bật trong tháng"
              list={listProductHighlightMonth}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default CartPage;

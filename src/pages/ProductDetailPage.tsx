import React, { useEffect } from "react";
import Description from "../components/Description/Description";
import PictureZoom from "../components/PictureZoom/PictureZoom";
import ProductDetail from "../components/ProductDetail/ProductDetail";
import ProductGrid from "../components/ProductCategory/ProductGrid";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "..";
import { useLocation, useParams } from "react-router";
import { fetchCategories, getProductById } from "../actions/product";

interface IParam {
  id: string;
}

const ProductDetailPage: React.FC = () => {
  const param = useParams<IParam>();
  const location = useLocation();
  const dispatch = useDispatch();
  const currentProduct = useSelector(
    (state: RootState) => state.product.currentProduct,
  );
  const message = useSelector(
    (state: RootState) => state.product.message.product,
  );
  const productListHighlightMonth = useSelector(
    (state: RootState) => state.product.highlightMonth,
  );

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  useEffect(() => {
    fetchCategories(dispatch, ["HighlightMonth"]);
  }, []);

  useEffect(() => {
    getProductById(dispatch, param.id);
  }, [param.id]);

  return (
    <main className="main-content">
      <div className="product-overview">
        <div className="container">
          {currentProduct ? (
            <div
              id="product-10"
              className="post-10 product-detail-page product type-product status-publish has-post-thumbnail product_cat-best-seller product_cat-drama product_tag-art product_tag-bio product_tag-business book_author-g-blakemore-evans book_publisher-boyds-mills-press book_series-series-4 first outofstock sale shipping-taxable purchasable product-type-variable"
            >
              <PictureZoom images={currentProduct.imageFromUrl || []} />
              <ProductDetail product={currentProduct} />
            </div>
          ) : (
            <h1 className="woocommerce-notices-wrapper">{message}</h1>
          )}
        </div>
      </div>
      {currentProduct && <Description product={currentProduct} />}
      <ProductGrid
        id="432932"
        name="Nổi bật trong tháng"
        list={productListHighlightMonth}
        limit={8}
      />
    </main>
  );
};

export default ProductDetailPage;

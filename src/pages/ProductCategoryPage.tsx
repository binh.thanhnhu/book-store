import React, { useEffect } from "react";
import { useLocation } from "react-router";
import PageHeader from "../components/PageHeader/PageHeader";
import ProductCategory from "../components/ProductCategory/ProductCategory";

const ProductCategoryPage: React.FC = () => {
  const location = useLocation();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  return (
    <>
      <PageHeader title="Shop" />
      <ProductCategory />
    </>
  );
};

export default ProductCategoryPage;

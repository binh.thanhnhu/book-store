import { fetchOrdersHistory } from "actions/user";
import PageHeader from "components/PageHeader/PageHeader";
import OrderTable from "components/YourOrder/OrderTable";
import { IOder } from "constants/interfaces";
import { RootState } from "index";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router";

const OrdersHistoryPage: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const token = useSelector((state: RootState) => state.user.token);
  const orderHistory = useSelector(
    (state: RootState) => state.user.orderHistory,
  );
  console.log(orderHistory);

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  useEffect(() => {
    if (!token) history.push("/");
    if (token && orderHistory?.length === 0)
      fetchOrdersHistory(dispatch, token);
  }, [token]);

  return (
    <>
      <PageHeader title="Lịch sử giao dịch" />
      <main className="main-content">
        <div className="container">
          <div className="content-area">
            {orderHistory &&
              orderHistory.map((order: IOder, i: number) => (
                <OrderTable key={i} productInCart={order} time={order.time} />
              ))}
          </div>
        </div>
      </main>
    </>
  );
};

export default OrdersHistoryPage;

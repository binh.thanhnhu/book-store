import PageHeader from "components/PageHeader/PageHeader";
import ProductCategory from "components/ProductCategory/ProductCategory";
import getParams from "utils/getParams";
import React, { useEffect } from "react";
import { useLocation } from "react-router";

const SearchPage: React.FC = () => {
  const location = useLocation();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  return (
    <>
      <PageHeader
        title="Tìm kiếm"
        content={`Kết quả tìm kiếm cho: ${getParams(location.search).keyword}`}
      />
      <ProductCategory search />
    </>
  );
};

export default SearchPage;

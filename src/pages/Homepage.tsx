import React, { useEffect } from "react";
import ProductGrid from "../components/ProductCategory/ProductGrid";
import { useDispatch, useSelector } from "react-redux";

import Banner from "../components/Banner/Banner";
import { fetchCategories } from "../actions/product";
import { RootState } from "..";
import { useLocation } from "react-router";

const Homepage: React.FC = () => {
  const productListBestSeller = useSelector(
    (state: RootState) => state.product.bestSeller,
  );
  const productListHighlightMonth = useSelector(
    (state: RootState) => state.product.highlightMonth,
  );
  const productListDiscount = useSelector(
    (state: RootState) => state.product.discount,
  );
  const productListNewest = useSelector(
    (state: RootState) => state.product.newest,
  );
  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  useEffect(() => {
    if (
      productListBestSeller.length === 0 ||
      productListHighlightMonth.length === 0 ||
      productListDiscount.length === 0 ||
      productListNewest.length === 0
    )
      fetchCategories(dispatch, [
        "BestSeller",
        "HighlightMonth",
        "Discount",
        "Newest",
      ]);
  }, []);

  return (
    <>
      <div className="menu-main-overlay"></div>
      <main className="main-content">
        <div className="kc_clfw"></div>
        <Banner />
        <ProductGrid
          id="514848"
          name="Bán chạy nhất"
          hasSmallProduct
          list={productListBestSeller}
          productListNewest={productListNewest}
          limit={8}
        />
        <ProductGrid
          id="351408"
          name="Khuyến mãi"
          fiveProduct
          list={productListDiscount}
          limit={5}
        />
        <ProductGrid
          id="232923"
          name="Nổi bật trong tháng"
          hasSmallProduct
          list={productListHighlightMonth}
          productListNewest={productListNewest}
          limit={8}
        />
      </main>
    </>
  );
};

export default Homepage;

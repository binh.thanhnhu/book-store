import PageHeader from "components/PageHeader/PageHeader";
import { RootState } from "index";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router";

const CheckoutPageSuccess: React.FC = () => {
  const message = useSelector(
    (state: RootState) => state.checkout.messageCheckoutSuccess,
  );
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, [location]);

  return message ? (
    <>
      <PageHeader title="Checkout" />
      <main className="main-content">
        <div className="container">
          <h1>{message}</h1>
        </div>
      </main>
    </>
  ) : (
    <>{history.push("/checkout")}</>
  );
};

export default CheckoutPageSuccess;

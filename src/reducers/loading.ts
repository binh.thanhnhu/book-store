import { AnyAction as Action } from "redux";
import { actionTypes } from "../constants/actionTypes";

interface IState {
  loading: boolean;
}

const defaultState = {
  loading: false,
};

export default function loading(
  state: IState = defaultState,
  action: Action,
): IState {
  switch (action.type) {
    case actionTypes.LOADING:
      return { ...state, loading: true };

    case actionTypes.STOP_LOADING:
      return { ...state, loading: false };

    default:
      return state;
  }
}

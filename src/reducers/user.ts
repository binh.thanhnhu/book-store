import { AnyAction as Action } from "redux";
import { actionTypes } from "constants/actionTypes";
import { IOder, IUserInfo } from "constants/interfaces";

interface IState {
  token: string;
  message: string;
  messageLogout: string;
  messageOrderHistory: string;
  messageUpdateUserInfoSuccess: string;
  messageUpdateUserInfoFailure: string;
  messageGetUserInfo: string;
  orderHistory: IOder[];
  userInfo: IUserInfo | null;
}

const defaultState = {
  token: localStorage.getItem("user_token") || "",
  message: "",
  messageLogout: "",
  messageOrderHistory: "",
  messageUpdateUserInfoSuccess: "",
  messageUpdateUserInfoFailure: "",
  messageGetUserInfo: "",
  orderHistory: [],
  userInfo: null,
};

export default function user(
  state: IState = defaultState,
  action: Action,
): IState {
  switch (action.type) {
    case actionTypes.LOGIN_SUCCESS:
      localStorage.setItem("user_token", action.payload.token);
      return { ...state, token: action.payload.token, message: "" };

    case actionTypes.LOGIN_FAILURE:
      return { ...state, message: action.payload.message };

    case actionTypes.REGISTER_SUCCESS:
      localStorage.setItem("user_token", action.payload.token);
      return { ...state, token: action.payload.token, message: "" };

    case actionTypes.REGISTER_FAILURE:
      return { ...state, message: action.payload.message };

    case actionTypes.LOGOUT_SUCCESS:
      localStorage.removeItem("user_token");
      return {
        ...state,
        ...defaultState,
        token: "",
      };

    case actionTypes.LOGOUT_FAILURE:
      return { ...state, messageLogout: action.payload.message };

    case actionTypes.RESET_MESSAGE_LOGIN_FORM:
      return { ...state, message: "" };

    case actionTypes.GET_ORDER_HISTORY_SUCCESS:
      return {
        ...state,
        orderHistory: action.payload.data,
        messageOrderHistory: action.payload.message,
      };

    case actionTypes.GET_ORDER_HISTORY_FAILURE:
      return { ...state, messageOrderHistory: action.payload.message };

    case actionTypes.UPDATE_USER_INFO_SUCCESS:
      return {
        ...state,
        messageUpdateUserInfoFailure: "",
        messageUpdateUserInfoSuccess: action.payload.message,
      };

    case actionTypes.UPDATE_USER_INFO_FAILURE:
      return {
        ...state,
        messageUpdateUserInfoFailure: action.payload.message,
        messageUpdateUserInfoSuccess: "",
      };

    case actionTypes.UPDATE_BILLING_DETAIL_FAILURE:
      return {
        ...state,
        messageUpdateUserInfoSuccess: "",
        messageUpdateUserInfoFailure: action.payload.message,
      };

    case actionTypes.GET_USER_INFO_SUCCESS:
      return {
        ...state,
        userInfo: action.payload.data,
      };

    case actionTypes.GET_USER_INFO_FAILURE:
      return {
        ...state,
        messageGetUserInfo: action.payload.message,
      };

    default:
      return state;
  }
}

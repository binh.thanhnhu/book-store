import { combineReducers } from "redux";
import form from "./form";
import user from "./user";
import loading from "./loading";
import product from "./product";
import menu from "./menu";
import cart from "./cart";
import checkout from "./checkout";

const rootReducer = combineReducers({
  form,
  user,
  loading,
  product,
  menu,
  cart,
  checkout,
});

export default rootReducer;

import { AnyAction as Action } from "redux";
import { actionTypes } from "../constants/actionTypes";

interface IState {
  openLogin: boolean;
}

const defaultState = {
  openLogin: false,
};

export default function form(
  state: IState = defaultState,
  action: Action,
): IState {
  switch (action.type) {
    case actionTypes.OPEN_LOGIN_FORM:
      return { ...state, openLogin: true };

    case actionTypes.CLOSE_LOGIN_FORM:
      return { ...state, openLogin: false };

    default:
      return state;
  }
}

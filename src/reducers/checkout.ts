import { AnyAction as Action } from "redux";
import { actionTypes } from "../constants/actionTypes";

interface IState {
  messageCheckout: string;
  messageCheckoutSuccess: string;
}

const defaultState = {
  messageCheckout: "",
  messageCheckoutSuccess: "",
};

export default function checkout(
  state: IState = defaultState,
  action: Action,
): IState {
  switch (action.type) {
    case actionTypes.CHECKOUT_SUCCESS:
      return {
        ...state,
        messageCheckoutSuccess: action.payload.message,
        messageCheckout: "",
      };

    case actionTypes.CHECKOUT_FAILURE:
      return {
        ...state,
        messageCheckout: action.payload.message,
        messageCheckoutSuccess: "",
      };

    default:
      return state;
  }
}

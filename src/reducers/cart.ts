import { AnyAction as Action } from "redux";
import totalPriceFunc from "utils/totalPriceFunc";
import { actionTypes } from "../constants/actionTypes";
import { IProductInCart } from "../constants/interfaces";

interface IState {
  productInCart: IProductInCart[];
  totalPrice: number;
  totalPriceTemp: number;
}

const productInCart = JSON.parse(localStorage.getItem("productInCart") || "[]");
const { total } = totalPriceFunc(productInCart);

const defaultState = {
  productInCart: JSON.parse(localStorage.getItem("productInCart") || "[]"),
  totalPrice: total,
  totalPriceTemp: total,
};

export default function cart(
  state: IState = defaultState,
  action: Action,
): IState {
  switch (action.type) {
    case actionTypes.ADD_TO_CART: {
      const tempProduct = [];
      const { _id } = action.payload.product;

      if (state.productInCart.length === 0)
        tempProduct.push(
          ...JSON.parse(localStorage.getItem("productInCart") || "[]"),
        );
      else tempProduct.push(...state.productInCart);

      if (tempProduct.length > 0) {
        for (let i = 0; i < tempProduct.length; i++) {
          if (tempProduct[i].product._id === _id) {
            tempProduct[i].quantity += action.payload.quantity;
            break;
          } else if (i === tempProduct.length - 1) {
            tempProduct.push({
              product: action.payload.product,
              quantity: action.payload.quantity,
            });
            break;
          }
        }
      } else {
        tempProduct.push({
          product: action.payload.product,
          quantity: action.payload.quantity,
        });
      }

      localStorage.setItem("productInCart", JSON.stringify(tempProduct));

      const { total } = totalPriceFunc(tempProduct);

      return {
        ...state,
        productInCart: tempProduct,
        totalPrice: total,
        totalPriceTemp: total,
      };
    }

    case actionTypes.REMOVE_PRODUCT_IN_CART: {
      let tempProduct = [];
      const _id = action.payload;

      if (state.productInCart.length === 0)
        tempProduct.push(
          ...JSON.parse(localStorage.getItem("productInCart") || "[]"),
        );
      else tempProduct.push(...state.productInCart);

      tempProduct = tempProduct.filter(
        (value: IProductInCart) => value.product._id !== _id,
      );

      localStorage.setItem("productInCart", JSON.stringify(tempProduct));

      const { total } = totalPriceFunc(tempProduct);

      return {
        ...state,
        productInCart: tempProduct,
        totalPrice: total,
        totalPriceTemp: total,
      };
    }

    case actionTypes.UPDATE_CART:
      localStorage.setItem(
        "productInCart",
        JSON.stringify(
          action.payload.productList.filter(
            (obj: IProductInCart) => obj.quantity !== 0,
          ),
        ),
      );

      return {
        ...state,
        productInCart: action.payload.productList.filter(
          (obj: IProductInCart) => obj.quantity !== 0,
        ),
        totalPrice: action.payload.totalPrice,
        totalPriceTemp: action.payload.totalPrice,
      };

    case actionTypes.SET_TOTAL_PRICE:
      return {
        ...state,
        totalPrice: action.payload,
      };

    case actionTypes.SET_TOTAL_PRICE_TEMP:
      return {
        ...state,
        totalPriceTemp: action.payload,
      };

    default:
      return state;
  }
}

import { AnyAction as Action } from "redux";
import { actionTypes } from "../constants/actionTypes";
import { IProduct } from "../constants/interfaces";
import lowercase from "../utils/lowercase";

interface IMessage {
  category: string;
  product: string;
  quickviewProduct: string;
  categories: string;
}

interface IState {
  category: IProduct[];
  totalPages: number;
  message: IMessage;
  currentProduct: IProduct | null;
  quickviewProduct: IProduct | null;
  isOpenQuickview: boolean;
  bestSeller: IProduct[];
  newest: IProduct[];
  highlightMonth: IProduct[];
  discount: IProduct[];
}

const defaultState = {
  category: [],
  totalPages: 1,
  message: {
    category: "",
    product: "",
    quickviewProduct: "",
    categories: "",
  },
  currentProduct: null,
  quickviewProduct: null,
  isOpenQuickview: false,
  bestSeller: [],
  newest: [],
  highlightMonth: [],
  discount: [],
};

export default function product(
  state: IState = defaultState,
  action: Action,
): IState {
  switch (action.type) {
    case actionTypes.GET_PRODUCT_OPTION_SUCCESS:
      return {
        ...state,
        category: action.payload.data.category,
        totalPages: action.payload.data.totalPages,
        message: { ...state.message, category: "" },
      };

    case actionTypes.GET_PRODUCT_OPTION_FAILURE:
      return {
        ...state,
        message: { ...state.message, category: action.payload.message },
        category: [],
        totalPages: 1,
      };

    case actionTypes.GET_PRODUCT_BY_ID_SUCCESS:
      return { ...state, currentProduct: action.payload.data };

    case actionTypes.GET_PRODUCT_BY_ID_FAILURE:
      return {
        ...state,
        currentProduct: null,
        message: { ...state.message, product: action.payload.message },
      };

    case actionTypes.GET_QUICKVIEW_PRODUCT_BY_ID_SUCCESS:
      return { ...state, quickviewProduct: action.payload.data };

    case actionTypes.GET_QUICKVIEW_PRODUCT_BY_ID_FAILURE:
      return {
        ...state,
        quickviewProduct: null,
        message: { ...state.message, quickviewProduct: action.payload.message },
      };

    case actionTypes.GET_PRODUCT_CATEGORIES_SUCCESS: {
      const obj: any = {};
      for (let i = 0; i < action.payload.length; i++) {
        obj[lowercase(action.payload[i as number].categoryName)] =
          action.payload[i].data.data;
      }
      return {
        ...state,
        ...obj,
        message: { ...state.message, categories: "" },
      };
    }

    case actionTypes.GET_PRODUCT_CATEGORIES_FAILURE:
      return {
        ...state,
        [lowercase(action.payload.categoryName)]: null,
        message: { ...state.message, categories: "Có lỗi xảy ra" },
      };

    case actionTypes.OPEN_QUICKVIEW:
      return {
        ...state,
        isOpenQuickview: action.payload.open,
        quickviewProduct: action.payload.product,
      };

    default:
      return state;
  }
}

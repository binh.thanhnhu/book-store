import { AnyAction as Action } from "redux";
import { actionTypes } from "../constants/actionTypes";

interface IState {
  menuActive: boolean;
  menuMobileActive: boolean;
}

const defaultState = {
  menuActive: false,
  menuMobileActive: false,
};

export default function menu(
  state: IState = defaultState,
  action: Action,
): IState {
  switch (action.type) {
    case actionTypes.OPEN_MENU:
      return { ...state, menuActive: action.payload };
    case actionTypes.OPEN_MENU_MOBILE:
      return {
        ...state,
        menuMobileActive: action.payload,
      };

    default:
      return state;
  }
}

import React from "react";
import { Link } from "react-router-dom";
import "./Banner.scss";

const Banner: React.FC = () => {
  return (
    <section className="kc-elm kc-css-579871 kc_row">
      <div className="kc-row-container">
        <div className="kc-wrap-columns">
          <div className="kc-elm kc-css-197136 kc_col-sm-12 kc_column kc_col-sm-12">
            <div className="kc-col-container">
              <div className="kc-elm kc-css-234997 kc_row kc_row_inner">
                <div className="kc-elm kc-css-284652 kc_col-sm-6 kc_column_inner kc_col-sm-6">
                  <div className="kc_wrapper kc-col-inner-container">
                    <div className="kc-elm kc-css-442500 kc_row kc_row_inner">
                      <div className="kc-elm kc-css-220987 kc_col-sm-5 kc_column_inner kc_col-sm-5">
                        <div className="kc_wrapper kc-col-inner-container">
                          <div className="kc-elm kc-css-702963 kc_shortcode kc_single_image kc-pc-loaded">
                            <img
                              src="https://res.cloudinary.com/binhnt13/image/upload/v1629429072/banner-2_m3fjnt.jpg"
                              className="loading"
                              alt=""
                            />{" "}
                          </div>
                        </div>
                      </div>
                      <div className="kc-elm kc-css-122680 kc_col-sm-7 kc_column_inner kc_col-sm-7">
                        <div className="kc_wrapper kc-col-inner-container">
                          <div className="kc-elm kc-css-547360 kc_shortcode kc_single_image kc-pc-loaded">
                            <img
                              src="https://res.cloudinary.com/binhnt13/image/upload/v1629428880/book-store/banner-1_gpu6nx.jpg"
                              className="loading"
                              alt=""
                            />{" "}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="kc-elm kc-css-747697 kc_col-sm-6 kc_column_inner kc_col-sm-6">
                  <div className="kc_wrapper kc-col-inner-container last">
                    <div className="kc-elm kc-css-317009 kc-title-wrap  kc-pc-loaded">
                      <h2 className="kc_title">
                        Tìm kiếm cuốn sách ưa thích của bạn
                      </h2>
                    </div>

                    <div className="kc-elm kc-css-58938 kc-title-wrap  kc-pc-loaded">
                      <h2 className="kc_title">Hàng triệu bộ sưu tập</h2>
                    </div>

                    <div className="kc-elm kc-css-627718 kc-pro-button kc-button-1 kc-pc-loaded">
                      <Link to="/product-category">
                        <span className="creative_title">Khám phá ngay</span>{" "}
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Banner;

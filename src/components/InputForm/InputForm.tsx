import classNames from "classnames";
import { FieldProps } from "formik";
import React from "react";

import "./InputForm.scss";

interface Props {
  tag: "input" | "textarea" | "select";
  name?: string;
  first?: boolean;
  last?: boolean;
  require?: boolean;
  className: string;
  children?: React.ReactNode;
  placeholder?: string;
  label: string;
  type?: string;
}

const InputForm: React.FC<Props & FieldProps> = ({
  field,
  form,
  tag,
  first,
  last,
  require = false,
  className,
  children,
  placeholder,
  label,
  type = "text",
}: Props & FieldProps) => {
  const { name, value, onChange } = field;
  const { errors, touched } = form;

  let html: React.ReactNode;

  switch (tag) {
    case "input":
      html = (
        <input
          type={type}
          className={className}
          id={name}
          name={name}
          placeholder={placeholder}
          value={value || ""}
          onChange={onChange}
        />
      );
      break;
    case "select":
      html = (
        <>
          <select
            id={name}
            name={name}
            className={className}
            tabIndex={-1}
            value={value || ""}
            onChange={onChange}
          >
            {children}
          </select>
        </>
      );
      break;
    case "textarea":
      html = (
        <textarea
          id={name}
          name={name}
          className={className}
          placeholder={placeholder}
          rows={2}
          cols={5}
          value={value || ""}
          onChange={onChange}
        />
      );
      break;
    default:
      null;
  }

  return (
    <p
      className={classNames("form-row", {
        "form-row-wide address-field": !first || !last,
        "form-row-first": first,
        "form-row-last": last,
        "validate-error": touched[name] && errors[name],
      })}
      id={`${name}_field`}
    >
      <label htmlFor={name} className="">
        {label}&nbsp;
        {require ? (
          <abbr className="required" title="required">
            *
          </abbr>
        ) : (
          <span className="optional">(optional)</span>
        )}
      </label>
      <span className="woocommerce-input-wrapper">{html}</span>
    </p>
  );
};

export default InputForm;

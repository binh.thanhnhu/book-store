import React, { useState } from "react";
import { IProduct } from "../../constants/interfaces";
import Star from "../Star/Star";
import Input from "../Input/Input";
import formatPrice from "../../utils/formatPrice";
import { addToCart } from "../../actions/cart";
import { useDispatch } from "react-redux";

interface Props {
  isQuickView?: boolean;
  product: IProduct;
}

const ProductDetail: React.FC<Props> = ({ isQuickView, product }: Props) => {
  const dispatch = useDispatch();
  const [qty, setQty] = useState<number>(1);

  const handleQty = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = event.currentTarget;
    (parseInt(value) >= 0 || value === "") &&
      setQty(parseInt(event.currentTarget.value));
  };

  const handleAddToCart = (
    event: React.FormEvent<HTMLFormElement>,
    qty: number,
  ): void => {
    event.preventDefault();
    addToCart(dispatch, product, qty);
  };

  return (
    <div className="product-overview__summary">
      {!isQuickView && product?.oldPrice && (
        <span className="onsale">Sale!</span>
      )}
      <h1 className="product__title">{product?.title}</h1>
      <div className="product__meta">
        <div className="author">
          {" "}
          <a href="#">{product?.author}</a>
        </div>
        <div className="woocommerce-product-rating">
          <div className="star-rating">{Star(product?.rated || 5)}</div>{" "}
          <a href="#reviews" className="woocommerce-review-link" rel="nofollow">
            (<span className="count">2</span> customer reviews)
          </a>
        </div>
      </div>
      <div className="product__excerpt">
        <div className="woocommerce-product-details__short-description">
          <p>{product?.description}</p>
        </div>
      </div>
      <div className="product-action">
        <p className="price">
          {product.oldPrice ? (
            <>
              <span className="woocommerce-Price-amount amount">
                <del>{formatPrice(product.oldPrice)}</del>
                <sup>đ</sup>
              </span>{" "}
              {"  "}
              <ins>
                <span className="woocommerce-Price-amount amount">
                  {formatPrice(product.price)}
                  <sup>đ</sup>
                </span>{" "}
              </ins>
            </>
          ) : (
            <span className="woocommerce-Price-amount amount">
              {formatPrice(product.price)}
              <sup>đ</sup>
            </span>
          )}
        </p>
        <form
          className="variations_form cart"
          onSubmit={(e) => handleAddToCart(e, qty)}
        >
          <div className="single_variation_wrap">
            <div
              className="woocommerce-variation single_variation"
              style={{ display: "none" }}
            />
            <div className="woocommerce-variation-add-to-cart variations_button woocommerce-variation-add-to-cart-disabled">
              <div className="quantity">
                <Input
                  tag="input"
                  type="number"
                  id="quantity_608bfca5ac4ee"
                  className="input-text qty text"
                  name="quantity"
                  value={qty}
                  title="Qty"
                  inputMode="numeric"
                  onChange={handleQty}
                />
              </div>
              <Input
                tag="button"
                type="submit"
                className="single_add_to_cart_button button alt disabled wc-variation-selection-needed"
              >
                Thêm vào giỏ
              </Input>
            </div>
          </div>
        </form>
        <div className="product-bookmark">
          <div className="clear" />{" "}
          <div className="post__share">
            <strong />
            <a href="#" className="facebook s_facebook">
              <i className="fa fa-facebook" />
              <span className="label">Facebook</span>
            </a>
            <a href="#" className="twitter s_twitter">
              <i className="fa fa-twitter" />
              <span className="label">Twitter</span>
            </a>
            <a href="#" className="google-plus s_plus">
              <i className="fa fa-google-plus" />
              <span className="label">Google+</span>
            </a>
            <a href="#" className="linkedin s_linkedin">
              <i className="fa fa-linkedin" />
              <span className="label">LinkedIn</span>
            </a>
          </div>
          <div className="separator separator--arrow" />
        </div>
        {/* <div className="product_meta">
          {product?.sku && (
            <span className="sku_wrapper">
              SKU: <span className="sku">{product?.sku}</span>
            </span>
          )}
          <span className="posted_in">
            Mục:{" "}
            <a href="#" rel="tag">
              Best Seller
            </a>
            ,{" "}
            <a href="#" rel="tag">
              Drama
            </a>
          </span>
          <span className="tagged_as">
            Tags:{" "}
            <a href="#" rel="tag">
              art
            </a>
            ,{" "}
            <a href="#" rel="tag">
              bio
            </a>
            ,{" "}
            <a href="#" rel="tag">
              business
            </a>
          </span>
        </div> */}
      </div>
    </div>
  );
};

export default ProductDetail;

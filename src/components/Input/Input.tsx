import React from "react";

type IButton = "button" | "submit" | "reset" | undefined;

interface Props {
  id?: string;
  tag: "button" | "input" | "select";
  type?: string;
  name?: string;
  value?: string | number;
  className?: string;
  children?: React.ReactNode;
  min?: number;
  max?: number;
  size?: number;
  step?: number;
  title?: string;
  inputMode?: "numeric";
  defaultValue?: string | number;
  placeholder?: string;
  required?: boolean;
  disabled?: boolean;
  onChange?: (e: any) => void;
  onClick?: (e: any) => void;
  onSubmit?: (e: any) => void;
}

const Input: React.FC<Props> = ({ tag, type, ...props }: Props) => {
  return tag === "button" ? (
    <button type={type as IButton} {...props}>
      {props.children}
    </button>
  ) : tag === "input" ? (
    <input type={type} {...props} />
  ) : (
    <select {...props}></select>
  );
};

export default Input;

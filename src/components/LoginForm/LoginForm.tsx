import React, { createRef, useState } from "react";
import InputForm from "../InputForm/InputForm";
import { useSelector, useDispatch } from "react-redux";
import {
  closeLoginForm,
  login,
  register,
  resetMessageLoginForm,
} from "../../actions/user";

import "./LoginForm.scss";
import Message from "../Message/Message";
import { RootState } from "../..";
import useClickOutside from "../../hooks/useClickOutside";
import { FastField, Form, Formik } from "formik";
import { DAccount } from "constants/init";
import { IAccount } from "constants/interfaces";
import { VAccountRegister, VAccountLogin } from "constants/validation";

const LoginForm: React.FC = () => {
  const [isRegister, setRegister] = useState<boolean>(false);

  const dispatch = useDispatch();
  const openLogin = useSelector((state: RootState) => state.form.openLogin);
  const message = useSelector((state: RootState) => state.user.message);

  const loginDiv = createRef<HTMLDivElement>();

  const handleCloseLoginForm = (): void => {
    closeLoginForm(dispatch);
  };

  const handleChangeToRegister = (
    event: React.FormEvent<EventTarget>,
    value: boolean,
    callback: () => void,
  ): void => {
    event.preventDefault();
    setRegister(value);
    callback();
  };

  const handleLoginForm = (values: IAccount, actions: any): void => {
    if (isRegister) {
      register(dispatch, values);
    } else {
      delete values.rePassword;
      login(dispatch, values);
    }
    actions.resetForm({ values });
  };

  useClickOutside(loginDiv, openLogin, () => closeLoginForm(dispatch));

  return (
    <div
      className="user-auth-box tokoo-popup"
      style={openLogin ? { display: "block" } : { display: "none" }}
    >
      <div className="user-auth-overlay" />
      <div
        ref={loginDiv}
        className="user-auth-box-content grid-layout columns-2"
      >
        <button className="tokoo-popup__close" onClick={handleCloseLoginForm}>
          <i className="dripicons-cross" />
        </button>
        <Formik
          initialValues={DAccount}
          validationSchema={isRegister ? VAccountRegister : VAccountLogin}
          onSubmit={handleLoginForm}
          enableReinitialize
        >
          {(formik) => (
            <Form className="login grid-item">
              <header className="section-header">
                <h2 className="section-title">
                  {isRegister ? "Đăng kí" : "Đăng nhập"}
                </h2>
              </header>
              {((Object.keys(formik.errors).length > 0 &&
                Object.keys(formik.touched).length > 0) ||
                (!formik.dirty && message)) && (
                <Message error>
                  {Object.keys(formik.errors).length > 0 ? (
                    <ul className="woocommerce-error">
                      {Object.keys(formik.errors).map((key, i) => (
                        <li key={i}>
                          {formik.touched[key as keyof IAccount] &&
                            formik.errors[key as keyof IAccount]}
                        </li>
                      ))}
                    </ul>
                  ) : (
                    !formik.dirty && message
                  )}
                </Message>
              )}
              <FastField
                tag="input"
                name="username"
                component={InputForm}
                require
                className="woocommerce-Input woocommerce-Input--text input-text"
                placeholder="Username hoặc Email"
                label="Tài khoản"
              />
              <FastField
                tag="input"
                name="password"
                component={InputForm}
                require
                className="woocommerce-Input woocommerce-Input--text input-text"
                placeholder="Nhập mật khẩu"
                label="Mật khẩu"
                type="password"
              />
              {isRegister && (
                <FastField
                  tag="input"
                  name="rePassword"
                  component={InputForm}
                  require
                  className="woocommerce-Input woocommerce-Input--text input-text"
                  placeholder="Nhập lại mật khẩu"
                  label="Nhập lại mật khẩu"
                  type="password"
                />
              )}

              <div className="login-action">
                <input
                  type="hidden"
                  id="woocommerce-login-nonce"
                  name="woocommerce-login-nonce"
                  defaultValue="d7b1e56916"
                />
                <input
                  type="hidden"
                  name="_wp_http_referer"
                  defaultValue="/pustaka/checkout/"
                />{" "}
                <label htmlFor="rememberme" className="inline">
                  <input
                    className="woocommerce-Input woocommerce-Input--checkbox"
                    name="rememberme"
                    type="checkbox"
                    id="rememberme"
                    defaultValue="forever"
                  />{" "}
                  Remember me{" "}
                </label>
                <input
                  type="submit"
                  className="woocommerce-Button button"
                  name="login"
                  value={isRegister ? "Đăng kí" : "Đăng nhập"}
                />
              </div>
              <div className="change-form">
                {isRegister ? (
                  <>
                    <span>Đã có tài khoản, </span>
                    <a
                      href="#"
                      onClick={(e) =>
                        handleChangeToRegister(e, false, () => {
                          formik.resetForm({
                            values: { username: "", password: "" },
                          });
                          resetMessageLoginForm(dispatch);
                        })
                      }
                    >
                      bấm vào đây
                    </a>
                  </>
                ) : (
                  <>
                    <span>Chưa có tài khoản, </span>
                    <a
                      href="#"
                      onClick={(e) =>
                        handleChangeToRegister(e, true, () => {
                          formik.resetForm({
                            values: {
                              username: "",
                              password: "",
                              rePassword: "",
                            },
                          });
                          resetMessageLoginForm(dispatch);
                        })
                      }
                    >
                      bấm vào đây
                    </a>
                  </>
                )}
              </div>
              <a className="lostpassword" href="#">
                Quên mật khẩu?
              </a>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default LoginForm;

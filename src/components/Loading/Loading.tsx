import React from "react";

import "./Loading.scss";

const Loading: React.FC = () => {
  return (
    <div id="preloader">
      <div id="loader"></div>
    </div>
  );
};

export default Loading;

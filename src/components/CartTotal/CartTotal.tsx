import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { RootState } from "../..";
import formatPrice from "../../utils/formatPrice";
import { setTotalPriceTemp } from "actions/cart";

import "./CartTotal.scss";

const CartTotal: React.FC = () => {
  const totalPriceTemp = useSelector(
    (state: RootState) => state.cart.totalPriceTemp,
  );
  const totalPrice = useSelector((state: RootState) => state.cart.totalPrice);
  const dispatch = useDispatch();

  useEffect(() => {
    setTotalPriceTemp(dispatch, totalPrice);
  }, []);

  return (
    <div className="cart-collaterals">
      <div className="cart_totals ">
        <header className="section-header">
          <h2 className="section-title">Tổng tiền</h2>
        </header>
        <table cellSpacing={0} className="shop_table shop_table_responsive">
          <tbody>
            <tr className="cart-subtotal">
              <th>Tạm tính</th>
              <td>
                <span className="woocommerce-Price-amount amount">
                  <span className="woocommerce-Price-currencySymbol">£</span>
                  {formatPrice(totalPriceTemp || 0)}
                  <sup>đ</sup>
                </span>
              </td>
            </tr>
            <tr className="order-total">
              <th>Tổng</th>
              <td>
                <strong>
                  <span className="woocommerce-Price-amount amount">
                    <span className="woocommerce-Price-currencySymbol">£</span>
                    {formatPrice(totalPriceTemp || 0)}
                    <sup>đ</sup>
                  </span>
                </strong>{" "}
              </td>
            </tr>
          </tbody>
        </table>
        <div className="wc-proceed-to-checkout">
          <Link
            to="/checkout"
            className="checkout-button button alt wc-forward"
          >
            Thanh Toán
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CartTotal;

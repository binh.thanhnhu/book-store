import React, { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router";
import addParams from "utils/addParams";
import "./Searchbox.scss";

interface Props {
  mobile?: boolean;
}

const Searchbox: React.FC<Props> = ({ mobile }: Props) => {
  const [keyword, setKeyword] = useState<string>("");
  const [label, setLabel] = useState<boolean>(true);
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    if (location.pathname !== "/search-result") {
      setKeyword("");
    }
  }, [location.pathname]);

  const handleSearch = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    history.push({
      pathname: "/search-result",
      search: addParams(location.search, { keyword }, ["type", "option"]),
    });
  };

  return (
    <form className="searchform" onSubmit={handleSearch}>
      <div className="product-search-input">
        <input
          id={mobile ? "product-search-keyword-mobile" : "product-search-title"}
          type="text"
          spellCheck="false"
          value={keyword}
          onChange={(e) => setKeyword(e.currentTarget.value)}
          onFocus={() => setLabel(false)}
          onBlur={() => setLabel(true)}
        />
        <label
          htmlFor={
            mobile ? "product-search-keyword-mobile" : "product-search-title"
          }
        >
          {label && !keyword && "Tìm kiếm tên sách"}
        </label>
        <button type="submit" className="search-icon">
          <i className="fa fa-search"></i>
        </button>
        <div className="line"></div>
      </div>
    </form>
  );
};

export default Searchbox;

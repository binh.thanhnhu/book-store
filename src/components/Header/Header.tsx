import React, { createRef, useState } from "react";

import "./Header.scss";
import Menu from "./Menu";
import Searchbox from "./Searchbox";
import CartHeader from "./CartHeader";
import MobileMenu from "./MobileMenu";
import LoginForm from "../LoginForm/LoginForm";
import { useDispatch, useSelector } from "react-redux";
import { fetchUserInfo, logout, openLoginForm } from "../../actions/user";
import userImage from "../../images/user.svg";
import classNames from "classnames";
import { RootState } from "../..";
import { useHistory } from "react-router";
import useClickOutside from "hooks/useClickOutside";
import { Link } from "react-router-dom";
import { useEffect } from "react";

const Header: React.FC = () => {
  const [userAcitve, setUserActive] = useState<boolean>(false);

  const history = useHistory();
  const dispatch = useDispatch();
  const token =
    useSelector((state: RootState) => state.user.token) ||
    localStorage.getItem("user_token") ||
    "";
  const user = useSelector((state: RootState) => state.user.userInfo);

  const accountDiv = createRef<HTMLDivElement>();

  useEffect(() => {
    fetchUserInfo(dispatch, token, false);
  }, [token]);

  const handleOpenLoginForm = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    openLoginForm(dispatch);
  };

  const handleLogout = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    logout(dispatch);
  };

  const handleGoToHomepage = () => () => {
    history.push("/");
  };

  const prevent = (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
  };

  useClickOutside(accountDiv, userAcitve, () => setUserActive(false));

  return (
    <>
      <div className="site-header-wrap is-sticky" style={{ height: 106 }}>
        <div className="site-header">
          <div className="container">
            <Menu />
            <div
              className="hdr-widget hdr-widget--site-logo"
              onClick={handleGoToHomepage()}
            >
              <div className="site-logo">
                <a
                  className="text-gradient"
                  href="#"
                  onClick={(e) => prevent(e)}
                >
                  <span style={{ color: "#cf39cd" }}>P</span>
                  <span style={{ color: "#d132b4" }}>u</span>
                  <span style={{ color: "#d32b9b" }}>s</span>
                  <span style={{ color: "#d52482" }}>t</span>
                  <span style={{ color: "#d71d69" }}>a</span>
                  <span style={{ color: "#d81650" }}>k</span>
                  <span style={{ color: "#db1037" }}>a</span>
                </a>
              </div>
              <small className="site-description">
                Just another Tokomoo site
              </small>
            </div>

            <div className="hdr-widget hdr-widget--product-search">
              <Searchbox />
            </div>

            <div ref={accountDiv} className="hdr-widget hdr-widget--menu-user">
              {token ? (
                <>
                  <button
                    className="button-wrap"
                    onClick={() => setUserActive(!userAcitve)}
                  >
                    <img
                      className="user-icon-image"
                      src={userImage}
                      alt="user"
                    />
                  </button>
                  <div
                    className={classNames("menu-main-wrapper right", {
                      "is-active": userAcitve,
                    })}
                  >
                    <ul className="menu">
                      <li className="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent user-info">
                        {user?.email}
                      </li>

                      <li className="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent">
                        <Link
                          to="/user-info"
                          onClick={() => setUserActive(false)}
                        >
                          Thông tin khách hàng
                        </Link>
                      </li>
                      <li className="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent">
                        <Link
                          to="/orders-history"
                          onClick={() => setUserActive(false)}
                        >
                          Lịch sử giao dịch
                        </Link>
                      </li>
                      <li className="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent">
                        <a href="#" onClick={handleLogout}>
                          Đăng xuất
                        </a>
                      </li>
                    </ul>
                  </div>
                </>
              ) : (
                <div className="menu-nologin-user-wrap">
                  <a
                    className="open-login-popup"
                    href="#"
                    onClick={handleOpenLoginForm}
                  >
                    Log In
                  </a>
                </div>
              )}
            </div>
            <CartHeader />
          </div>
          <MobileMenu />
        </div>
      </div>
      <LoginForm />
    </>
  );
};

export default Header;

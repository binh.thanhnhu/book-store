import React, { createRef, useEffect, useState } from "react";
import classNames from "classnames";
import { useHistory, useLocation } from "react-router";
import { BookType } from "../../constants/data";

import "./Menu.scss";
import addParams from "../../utils/addParams";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../..";
import { openMenu, openMenuMobile } from "../../actions/menu";
import useClickOutside from "../../hooks/useClickOutside";

const Menu: React.FC<any> = () => {
  const history = useHistory();
  const location = useLocation();

  const [width, setWidth] = useState<number>(window.innerWidth);
  const menuDiv = createRef<HTMLDivElement>();

  const menuActive = useSelector((state: RootState) => state.menu.menuActive);
  const menuMobileActive = useSelector(
    (state: RootState) => state.menu.menuMobileActive,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    function handleResize() {
      setWidth(window.innerWidth);
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const toggleMenu = (): void => {
    console.log(menuMobileActive);
    openMenu(dispatch, !menuActive);
    openMenuMobile(dispatch, !menuMobileActive);
  };

  const handleGoToCategory = (
    event: React.FormEvent<EventTarget>,
    type: string,
    option?: string,
  ): void => {
    event.stopPropagation();

    const page = 1;
    const url = addParams(location.search, { type, option, page }, ["keyword"]);
    history.push({
      pathname: "/product-category",
      search: url,
    });

    openMenu(dispatch, false);
  };

  useClickOutside(menuDiv, menuActive && width >= 991, () =>
    openMenu(dispatch, false),
  );

  return (
    <div
      ref={menuDiv}
      className="hdr-widget hdr-widget--menu-main open-onclick"
    >
      <button
        onClick={toggleMenu}
        className={classNames("menu-main-toggle hamburger hamburger--elastic", {
          "is-active": menuActive,
        })}
        type="button"
      >
        <span className="hamburger-box">
          <span className="hamburger-inner"></span>
        </span>
      </button>
      <div
        className={classNames("menu-main-wrapper", {
          "is-active": menuActive,
        })}
      >
        <ul id="menu-primary-menus" className="menu">
          {BookType.map((o) => (
            <li
              key={o.id}
              id={o.id}
              className={classNames(
                "menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent",
                { "menu-item-has-children": !!o.menu },
              )}
              onClick={(e) => handleGoToCategory(e, o.type)}
            >
              <a>
                <i className="icomoon-menu2"></i>
                <span className="menu-label">{o.name}</span>
              </a>
              <ul className="sub-menu level-0">
                {o.menu &&
                  o.menu.map((o2) => (
                    <li
                      key={o2.id}
                      id={o2.id}
                      className="menu-item menu-item-type-post_type menu-item-object-page"
                      onClick={(e) => handleGoToCategory(e, o.type, o2.option)}
                    >
                      <a href={o2.href}>
                        <span className="menu-label">{o2.name}</span>
                      </a>
                    </li>
                  ))}
              </ul>
            </li>
          ))}
        </ul>
        <div className="menu-background">
          <div className="menu-main-background" style={{ width: 200 }}></div>
          <div className="sub-bg-container" style={{ left: 202 }}></div>
        </div>
      </div>
    </div>
  );
};

export default Menu;

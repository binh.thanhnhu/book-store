import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import totalPriceFunc from "utils/totalPriceFunc";
import { RootState } from "../..";
import { removeProductInCart } from "../../actions/cart";
import formatPrice from "../../utils/formatPrice";

import "./CartHeader.scss";

const CartHeader: React.FC = () => {
  const dispatch = useDispatch();
  const productInCart = useSelector(
    (state: RootState) => state.cart.productInCart,
  );

  const [totalPrice, setTotalPrice] = useState<number>(0);
  const [num, setNum] = useState<number>(0);

  useEffect(() => {
    const { total, n } = totalPriceFunc(productInCart);
    setTotalPrice(total);
    setNum(n);
  }, [productInCart]);

  const handleRemoveProductInCart = (
    event: React.FormEvent<EventTarget>,
    productId: string,
  ): void => {
    event.preventDefault();
    removeProductInCart(dispatch, productId);
  };

  return (
    <div className="hdr-widget hdr-widget--menu-cart">
      <div className="menu-cart">
        <button className="menu-cart-trigger">
          <span className="text-gradient">
            <span style={{ color: "#ce3cd9" }}>G</span>
            <span style={{ color: "#cf39cd" }}>i</span>
            <span style={{ color: "#cf35c0" }}>ỏ</span>
            <span style={{ color: "#d132b4" }}> </span>
            <span style={{ color: "#d22ea7" }}>H</span>
            <span style={{ color: "#d32b9b" }}>à</span>
            <span style={{ color: "#d4288e" }}>n</span>
            <span style={{ color: "#d52482" }}>g</span>
            <span style={{ color: "#d62175" }}> </span>
            <span style={{ color: "#d71d69" }}> </span>
            <span style={{ color: "#d81a5c" }}> </span>
            <span style={{ color: "#d91650" }}> </span>
            <span style={{ color: "#da1343" }}> </span>
            <span style={{ color: "#da1037" }}> </span>
          </span>
          <span className="cart-count">{num}</span>
        </button>
        <div className="widget woocommerce widget_shopping_cart">
          <h2 className="widgettitle">Giỏ hàng</h2>
          <div className="widget_shopping_cart_content">
            {productInCart.length > 0 ? (
              <>
                <ul className="cart_list product_list_widget ">
                  {productInCart.map((o, i) => (
                    <li key={i} className="mini_cart_item">
                      <a
                        href="#"
                        className="remove"
                        title="Remove this item"
                        onClick={(e) =>
                          handleRemoveProductInCart(e, o.product._id)
                        }
                      >
                        <i className="fas fa-trash"></i>
                      </a>
                      <figure className="product-image">
                        <Link to={`/product/${o.product._id}`}>
                          <img
                            width="330"
                            height="452"
                            src={o.product.imageFromUrl[0]?.url}
                            className="attachment-shop_catalog size-shop_catalog loading"
                            alt=""
                          />{" "}
                        </Link>
                      </figure>
                      <div className="product-detail">
                        <Link to={`/product/${o.product._id}`}>
                          {o.product.title}
                        </Link>
                        <span className="quantity">
                          {`${o.quantity} x `}
                          <span className="woocommerce-Price-amount amount">
                            {formatPrice(o.product.price)}
                            <sup>đ</sup>
                          </span>
                        </span>{" "}
                      </div>
                    </li>
                  ))}
                </ul>
                <p className="total">
                  <strong>Tổng:</strong>{" "}
                  <span className="woocommerce-Price-amount amount">
                    {formatPrice(totalPrice)}
                    <sup>đ</sup>
                  </span>
                </p>

                <p className="buttons">
                  <Link to="/cart" className="button wc-forward">
                    Xem Giỏ Hàng
                  </Link>
                  <Link to="/checkout" className="button checkout wc-forward">
                    Thanh Toán
                  </Link>
                </p>
              </>
            ) : (
              <ul>
                <li className="empty">No products in the cart.</li>
              </ul>
            )}
          </div>
        </div>{" "}
      </div>
    </div>
  );
};

export default CartHeader;

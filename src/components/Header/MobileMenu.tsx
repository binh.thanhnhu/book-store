import { openLoginForm } from "actions/user";
import classNames from "classnames";
import SearchBox from "components/Header/Searchbox";
import React, { createRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { RootState } from "../..";
import { BookType } from "../../constants/data";
import addParams from "../../utils/addParams";
import "./MobileMenu.scss";

const MobileMenu: React.FC = () => {
  const [subMenuActive, setSubMenuActive] = useState<{
    [index: string]: boolean;
  }>({});

  const menuMobile = createRef<HTMLDivElement>();

  const history = useHistory();
  const dispatch = useDispatch();

  const menuMobileActive = useSelector(
    (state: RootState) => state.menu.menuMobileActive,
  );

  const openSubMenu = (index: number): any => {
    setSubMenuActive({ ...subMenuActive, [index]: !subMenuActive[index] });
  };

  const prevent = (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
  };

  const handleOpenLoginForm = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    openLoginForm(dispatch);
  };

  const handleGoToCategory = async (
    event: React.FormEvent<EventTarget>,
    type: string,
    option?: string,
  ): Promise<void> => {
    event.stopPropagation();

    const page = 1;
    history.push({
      pathname: "/product-category",
      search: addParams(location.search, { type, option, page }, ["keyword"]),
    });
  };

  return (
    <div
      ref={menuMobile}
      className="mobile-menu-wrap"
      style={menuMobileActive ? { display: "block" } : { display: "none" }}
    >
      <div className="hdr-widget--product-search">
        <SearchBox mobile />
      </div>
      <nav className="mobile-menu">
        <ul id="menu-primary-menus" className="menu">
          {BookType.map((o, i) => (
            <li
              key={o.id}
              id={`menu-item-${o.id}`}
              className={classNames(
                "menu-item-mobile menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children",
                { active: subMenuActive[i] },
              )}
              onClick={() => openSubMenu(i)}
            >
              <a href="#" onClick={(e) => prevent(e)}>
                <i className="icomoon-menu2"></i>
                <span
                  className="menu-label"
                  onClick={(e) => handleGoToCategory(e, o.type)}
                >
                  {o.name}
                </span>
                {o.menu && (
                  <button
                    className={classNames("open-sub", {
                      active: subMenuActive[i],
                    })}
                  >
                    {subMenuActive[i] ? (
                      <i className="fas fa-chevron-up"></i>
                    ) : (
                      <i className="fas fa-chevron-down"></i>
                    )}
                  </button>
                )}
              </a>
              <ul className="sub-menu level-0">
                {o.menu &&
                  o.menu.map((o2, i2) => (
                    <li
                      key={o2.id}
                      id={`menu-item-${o2.id}`}
                      className="menu-item menu-item-type-post_type menu-item-object-page"
                      style={{
                        animation: `translateX 300ms ${
                          60 * (i2 + 1)
                        }ms ease-in-out forwards`,
                      }}
                    >
                      <a
                        href={o2.href}
                        onClick={(e) =>
                          handleGoToCategory(e, o.type, o2.option)
                        }
                      >
                        <span className="menu-label">{o2.name}</span>
                      </a>
                    </li>
                  ))}
              </ul>
            </li>
          ))}
        </ul>
      </nav>

      <div className="hdr-widget hdr-widget--menu-user">
        <div className="menu-nologin-user-wrap">
          <a
            className="open-login-popup"
            href="#"
            onClick={handleOpenLoginForm}
          >
            Log In
          </a>
        </div>
      </div>
    </div>
  );
};

export default MobileMenu;

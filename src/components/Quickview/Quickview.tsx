import React, { createRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../..";
import { openQuickview } from "../../actions/product";
import { IProduct } from "../../constants/interfaces";
import useClickOutside from "../../hooks/useClickOutside";
import PictureZoom from "../PictureZoom/PictureZoom";
import ProductDetail from "../ProductDetail/ProductDetail";

import "./Quickview.scss";

interface Props {
  product: IProduct;
}

const Quickview: React.FC<Props> = ({ product }: Props) => {
  const isOpen = useSelector(
    (state: RootState) => state.product.isOpenQuickview,
  );
  const dispatch = useDispatch();

  const quickviewDiv = createRef<HTMLDivElement>();

  const closeModal = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    openQuickview(dispatch, false, null);
  };

  useClickOutside(quickviewDiv, isOpen, () =>
    openQuickview(dispatch, false, null),
  );

  if (!isOpen) return null;

  return (
    <div id="yith-quick-view-modal" className="open">
      <div className="yith-quick-view-overlay" />
      <div ref={quickviewDiv} className="yith-wcqv-wrapper">
        <div className="yith-wcqv-main">
          <div className="yith-wcqv-head">
            <a
              href="#"
              id="yith-quick-view-close"
              className="yith-wcqv-close"
              onClick={(e) => closeModal(e)}
            >
              X
            </a>
          </div>
          <div
            id="yith-quick-view-content"
            className="woocommerce single-product"
          >
            <div
              id="product-10"
              className="post-10 type-product status-publish has-post-thumbnail product_cat-best-seller product_cat-drama product_tag-art product_tag-bio product_tag-business book_author-g-blakemore-evans book_publisher-boyds-mills-press book_series-series-4 product first outofstock sale shipping-taxable purchasable product-type-variable"
            >
              <div className="product-overview">
                <span className="onsale">Sale!</span>
                <PictureZoom images={product.imageFromUrl || []} />
                <ProductDetail isQuickView product={product} />
                <meta itemProp="url" content="#" />
              </div>
            </div>
            {/* #product-10 */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Quickview;

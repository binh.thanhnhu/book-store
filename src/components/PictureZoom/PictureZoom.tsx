import { IProduct } from "constants/interfaces";
import React, { useState } from "react";

import "./PictureZoom.scss";

interface Props {
  images: IProduct["imageFromUrl"];
}

const PictureZoom: React.FC<Props> = ({ images }: Props) => {
  const [selectedImage, setSelectedImage] = useState<number>(0);
  const handleChooseImage = (id: number) => (): void => {
    setSelectedImage(id);
  };

  return (
    <div
      className="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images"
      style={{
        opacity: 1,
        transition: "opacity 0.25s ease-in-out 0s",
      }}
    >
      <figure className="woocommerce-product-gallery__wrapper">
        <div
          className="woocommerce-product-gallery__image"
          style={{ position: "relative", overflow: "hidden" }}
        >
          <a href="#">
            {images.map((img, i) => (
              <img
                key={i}
                src={img.url}
                className="wp-post-image"
                alt=""
                style={
                  selectedImage === i
                    ? { display: "block" }
                    : { display: "none" }
                }
              />
            ))}
          </a>
        </div>{" "}
        <div className="slider">
          {images.map((img, i) => (
            <img
              key={i}
              src={img.url}
              className="img-selector"
              alt=""
              style={{ display: "inline" }}
              onClick={handleChooseImage(i)}
            />
          ))}
        </div>
      </figure>
    </div>
  );
};

export default PictureZoom;

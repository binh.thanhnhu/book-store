import React, { useState } from "react";
import classNames from "classnames";
import { IProduct } from "../../constants/interfaces";

import "./Description.scss";
import moment from "moment";

interface Props {
  product: IProduct;
}

const Description: React.FC<Props> = ({ product }: Props) => {
  const [active, setActive] = useState<{ [index: string]: boolean }>({
    1: true,
  });

  const handleActive = (
    event: React.FormEvent<EventTarget>,
    obj: { [index: string]: boolean },
  ): void => {
    event.preventDefault();
    setActive(obj);
  };

  return (
    <div className="product-detail">
      <div className="woocommerce-tabs wc-tabs-wrapper">
        <div className="product__detail-nav">
          <div className="container">
            <ul className="tabs wc-tabs">
              <li
                className={classNames("description_tab", { active: active[1] })}
              >
                <a onClick={(e) => handleActive(e, { 1: true })} href="#">
                  Mô tả
                </a>
              </li>
              <li
                className={classNames("additional_information_tab", {
                  active: active[2],
                })}
              >
                <a onClick={(e) => handleActive(e, { 2: true })} href="#">
                  Thông tin
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="product__detail-content">
          <div
            className="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab"
            style={active[1] ? { display: "block" } : { display: "none" }}
          >
            <div className="kc_clfw"></div>
            <section className="kc-elm kc-css-515605 kc_row">
              <div className="kc-row-container  kc-container">
                <div className="kc-wrap-columns">
                  <div className="kc-elm kc-css-928595 kc_col-sm-12 kc_column kc_col-sm-12">
                    <div className="kc-col-container">
                      <div className="kc-elm kc-css-255659 kc_text_block">
                        {product.description2 || product.description}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <div
            className="woocommerce-Tabs-panel woocommerce-Tabs-panel--additional_information panel entry-content wc-tab"
            style={active[2] ? { display: "block" } : { display: "none" }}
          >
            <div className="container">
              <table className="shop_attributes">
                <tbody>
                  <tr>
                    <th>Ngày xuất bản</th>
                    <td>
                      <p>
                        {moment(product.dateOfPublication).format("DD-MM-YYYY")}
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <th>Số trang</th>
                    <td>
                      <p>{product.numberOfPages}</p>
                    </td>
                  </tr>
                  <tr>
                    <th>SKU</th>
                    <td>
                      <p>{product.SKU}</p>
                    </td>
                  </tr>
                  <tr>
                    <th>Tác giả</th>
                    <td>
                      <p>{product.author}</p>
                    </td>
                  </tr>
                  <tr className="">
                    <th>Kích thước</th>
                    <td>
                      <p>{product.size}</p>
                    </td>
                  </tr>
                  <tr className="">
                    <th>Công ty phát hành</th>
                    <td>
                      <p>{product.publishingCompany}</p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>{" "}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Description;

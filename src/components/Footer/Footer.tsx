import React from "react";
import Input from "../Input/Input";

import "./Footer.scss";

const Footer: React.FC = () => {
  return (
    <section className="kc-elm kc-css-303509 kc_row">
      <div className="kc-row-container  kc-container">
        <div className="kc-wrap-columns">
          <div className="kc-elm kc-css-739807 kc_col-sm-12 kc_column kc_col-sm-12">
            <div className="kc-col-container">
              <div className="kc-elm kc-css-169322 kc_row kc_row_inner">
                <div className="kc-elm kc-css-430541 kc_col-sm-4 kc_column_inner kc_col-sm-4">
                  <div className="kc_wrapper kc-col-inner-container">
                    <div className="kc-elm kc-css-419334 kc-icon-wrapper">
                      <i className="fas fa-phone"></i>
                    </div>
                  </div>
                </div>
                <div className="kc-elm kc-css-828592 kc_col-sm-8 kc_column_inner kc_col-sm-8">
                  <div className="kc_wrapper kc-col-inner-container">
                    <div className="kc-elm kc-css-901472 kc-title-wrap ">
                      <h2 className="kc_title">+62 8798 6368 486</h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className="kc-elm kc-css-639366 kc-title-wrap ">
                <span className="kc_title">
                  7 Ngày một tuần từ 10h am đến 6h pm
                </span>
              </div>
              <div className="kc-elm kc-css-976337 kc_row kc_row_inner">
                <div className="kc-elm kc-css-349345 kc_col-sm-5 kc_column_inner kc_col-sm-5">
                  <div className="kc_wrapper kc-col-inner-container">
                    {" "}
                    <div className="section-header kc-elm kc-css-831376 section-title-style section-header--left">
                      <h2 className="section-title">Newsletter Signup</h2>
                      <small className="section-subtitle" />
                    </div>
                  </div>
                </div>
                <div className="kc-elm kc-css-842950 kc_col-sm-7 kc_column_inner kc_col-sm-7">
                  <div className="kc_wrapper kc-col-inner-container">
                    <div className="kc-elm kc-css-979111 section-title-style">
                      <form
                        id="mc4wp-form-1"
                        className="mc4wp-form mc4wp-form-95"
                        method="post"
                      >
                        <div className="mc4wp-form-fields">
                          <p>
                            <label>Email address: </label>
                            <Input
                              tag="input"
                              type="email"
                              name="EMAIL"
                              placeholder="Your email address"
                              required
                            />
                          </p>
                          <p>
                            <input type="submit" value="Sign up" />
                          </p>
                        </div>
                        <label style={{ display: "none" }}>
                          Leave this field empty if you&apos;re human:{" "}
                          <input
                            type="text"
                            name="_mc4wp_honeypot"
                            defaultValue=""
                            tabIndex={-1}
                            autoComplete="off"
                          />
                        </label>
                        <input
                          type="hidden"
                          name="_mc4wp_timestamp"
                          defaultValue={1619841429}
                        />
                        <input
                          type="hidden"
                          name="_mc4wp_form_id"
                          defaultValue={95}
                        />
                        <input
                          type="hidden"
                          name="_mc4wp_form_element_id"
                          defaultValue="mc4wp-form-1"
                        />
                        <div className="mc4wp-response" />
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div className="kc-elm kc-css-782366 kc-multi-icons-wrapper pustaka-custom-css-social-icon">
                <a
                  href="#"
                  target="_blank"
                  title="Facebook"
                  className="multi-icons-link multi-iconsfa-facebook"
                >
                  <i className="fa-facebook" />
                </a>
                <a
                  href="#"
                  target="_blank"
                  title="Twitter"
                  className="multi-icons-link multi-iconsfa-twitter"
                >
                  <i className="fa-twitter" />
                </a>
                <a
                  href="#"
                  target="_blank"
                  title="Google+"
                  className="multi-icons-link multi-iconsfa-google-plus"
                >
                  <i className="fa-google-plus" />
                </a>
                <a
                  href="#"
                  target="_blank"
                  title="Linkedin"
                  className="multi-icons-link multi-iconsfa-linkedin"
                >
                  <i className="fa-linkedin" />
                </a>
                <a
                  href="#"
                  target="_blank"
                  title="Youtube"
                  className="multi-icons-link multi-iconsfa-youtube"
                >
                  <i className="fa-youtube" />
                </a>
              </div>
              <div className="kc-elm kc-css-350475 kc-title-wrap ">
                <span className="kc_title">
                  Pustaka © 2021 All Rights Reserved{" "}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Footer;

import { IOder, IOderProduct } from "constants/interfaces";
import React from "react";
import formatPrice from "utils/formatPrice";
import moment from "moment";
import classNames from "classnames";

import "./OrderTable.scss";

interface Props {
  productInCart: IOder;
  totalPrice?: number;
  time?: Date;
  title?: string;
}

const OrderTable: React.FC<Props> = ({
  productInCart,
  totalPrice,
  time,
  title = "Hóa đơn",
}: Props) => {
  let totalPrice2 = 0;
  if (!totalPrice) {
    totalPrice2 = productInCart.info.reduce(
      (total, current) => total + current.price * current.quantity,
      0,
    );
  }

  return (
    <>
      <div className="section-header">
        <h3
          id="order_review_heading"
          className={classNames("section-title", { date: !!time })}
        >
          {time ? moment(time).locale("vi").format("LLL") : title}
        </h3>
      </div>
      <table className="shop_table woocommerce-checkout-review-order-table">
        <thead>
          <tr>
            <th className="product-name">Sản phẩm</th>
            <th className="product-total">Tổng</th>
          </tr>
        </thead>
        <tbody>
          {productInCart.info.map((o: IOderProduct) => (
            <tr key={`order-${o.productId}`} className="cart_item">
              <td className="product-name">
                {`${o.productTitle}  `}&nbsp;
                <strong className="product-quantity">
                  {`× ${o.quantity}`}
                </strong>{" "}
              </td>
              <td className="product-total">
                <span className="woocommerce-Price-amount amount">
                  {formatPrice(o.price * o.quantity)}
                  <sup>đ</sup>
                </span>{" "}
              </td>
            </tr>
          ))}
        </tbody>
        <tfoot>
          <tr className="cart-subtotal">
            <th>Subtotal</th>
            <td>
              <span className="woocommerce-Price-amount amount">
                {formatPrice(totalPrice2 || totalPrice || 0)}
                <sup>đ</sup>
              </span>
            </td>
          </tr>
          <tr className="order-total">
            <th>Total</th>
            <td>
              <strong>
                <span className="woocommerce-Price-amount amount">
                  {formatPrice(totalPrice2 || totalPrice || 0)}
                  <sup>đ</sup>
                </span>
              </strong>{" "}
            </td>
          </tr>
        </tfoot>
      </table>
    </>
  );
};

export default OrderTable;

import { IProductInCart } from "constants/interfaces";
import { RootState } from "index";
import React from "react";
import { useSelector } from "react-redux";
import OrderTable from "./OrderTable";
import "./YourOrder.scss";

const YourOrder: React.FC = () => {
  const productInCart = useSelector(
    (state: RootState) => state.cart.productInCart,
  );
  const totalPrice = useSelector((state: RootState) => state.cart.totalPrice);

  const order: any = {};
  order.info = productInCart.map((product: IProductInCart) => {
    return {
      productId: product.product._id,
      productTitle: product.product.title,
      quantity: product.quantity,
      price: product.product.price,
    };
  });

  return (
    <div id="order_review" className="woocommerce-checkout-review-order">
      <OrderTable
        productInCart={order}
        totalPrice={totalPrice}
        title="Hóa đơn"
      />
      <div id="payment" className="woocommerce-checkout-payment">
        <div className="form-row place-order">
          <noscript>
            Since your browser does not support JavaScript, or it is disabled,
            please ensure you click the &lt;em&gt;Update Totals&lt;/em&gt;
            button before placing your order. You may be charged more than the
            amount stated above if you fail to do so. &lt;br/&gt;&lt;button
            type=&quot;submit&quot; class=&quot;button alt&quot;
            name=&quot;woocommerce_checkout_update_totals&quot;
            value=&quot;Update totals&quot;&gt;Update totals&lt;/button&gt;
          </noscript>
          <div className="woocommerce-terms-and-conditions-wrapper">
            <div className="woocommerce-privacy-policy-text" />
          </div>
          <button
            type="submit"
            className="button alt"
            id="place_order"
            value="Place order"
          >
            Place order
          </button>
        </div>
      </div>
    </div>
  );
};

export default YourOrder;

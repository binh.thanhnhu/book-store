import React from "react";

import "./PageHeader.scss";

interface Props {
  title: string;
  content?: string;
}

const PageHeader: React.FC<Props> = ({ title, content }: Props) => {
  return (
    <div className="page-header page-header--center">
      <div className="page-header-bg">
        <div
          className="bg"
          style={{
            backgroundImage: 'url("")',
            transform: "matrix(1, 0, 0, 1, 0, 0)",
          }}
        />
      </div>
      <div className="container">
        <h2 className="page-title">{title}</h2>
        <p>{content}</p>
      </div>
    </div>
  );
};

export default PageHeader;

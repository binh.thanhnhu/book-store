import React from "react";
import List from "./List";
import { IProduct } from "../../constants/interfaces";

import "./ProductList.scss";

interface Props {
  list: IProduct[];
  name: string;
  id: string;
}

const ProductList: React.FC<Props> = ({ list, name, id }: Props) => {
  return (
    <div className="kc-product-list">
      <div className={`kc-elm kc-css-${id} kc-title-wrap`}>
        <h3 className="kc_title">
          <a href="#" className="kc_title_link">
            {name}
          </a>
        </h3>
      </div>
      <div className="kc-elm kc-css-989445 divider_line">
        <div className="divider_inner divider_line1" />
      </div>
      <div className="product-list">
        {list.map((o: IProduct) => (
          <List key={o._id} product={o} />
        ))}
      </div>
    </div>
  );
};

export default ProductList;

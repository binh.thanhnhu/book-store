import Star from "components/Star/Star";
import React from "react";
import formatPrice from "utils/formatPrice";

import "./SmallProduct.scss";

interface Props {
  img?: string;
  title: string;
  rated?: number;
  price: number;
}

const SmallProduct: React.FC<Props> = ({ img, title, rated, price }: Props) => {
  return (
    <li>
      <a href="#" className="product-image">
        <img
          width={330}
          height={452}
          src={img}
          className="attachment-woocommerce_thumbnail size-woocommerce_thumbnail loading"
          alt=""
        />{" "}
      </a>
      <div className="product-detail">
        <a href="#" title={title}>
          <span className="product-title">{title}</span>
        </a>
        <div className="star-rating">{Star(rated || 5)}</div>
        <span className="woocommerce-Price-amount amount">
          {formatPrice(price)}
          <sup>đ</sup>
        </span>{" "}
      </div>
    </li>
  );
};

export default SmallProduct;

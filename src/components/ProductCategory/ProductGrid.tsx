import React from "react";
import Product from "./Product";
import SmallProduct from "./SmallProduct";

import { IProduct } from "../../constants/interfaces";

import "./ProductGrid.scss";
import classNames from "classnames";

interface Props {
  id: string;
  name?: string;
  fiveProduct?: boolean;
  hasSmallProduct?: boolean;
  category?: boolean;
  list: IProduct[];
  productListNewest?: IProduct[];
  limit?: number;
}

const ProductGrid: React.FC<Props> = ({
  id,
  name,
  fiveProduct,
  hasSmallProduct,
  category,
  list,
  productListNewest,
  limit,
}: Props) => {
  return (
    <section className={`kc-elm kc-css-${id} kc_row kc-pc-loaded`}>
      <div className="kc-row-container kc-container">
        <div className="kc-wrap-columns">
          {fiveProduct && (
            <div className="kc-elm kc-css-843242 kc_col-of-5 kc_column kc_col-of-5">
              <div className="kc-col-container"></div>
            </div>
          )}
          <div
            className={classNames(
              "kc-elm kc_col-sm-10 kc_column kc_col-sm-10",
              { "kc-css-460138": !category },
            )}
          >
            <div className="kc-col-container">
              <div className="kc-elm kc-css-981637 kc-title-wrap ">
                <h3 className="kc_title">
                  <a href="#" className="kc_title_link">
                    {name}
                  </a>
                </h3>
              </div>
              <div className="kc-elm kc-css-989445 divider_line">
                <div className="divider_inner divider_line1"></div>
              </div>
              {fiveProduct ? (
                <div className="product-grid grid-layout columns-5">
                  {(list || []).map(
                    (o: IProduct, i: number) =>
                      i < 5 && <Product key={o._id} product={o} />,
                  )}
                </div>
              ) : limit ? (
                <div
                  key={0}
                  className="product-grid grid-layout columns-4 limit"
                >
                  {(list || []).map((o: IProduct, i: number) =>
                    limit ? (
                      i < limit && <Product key={o._id} product={o} />
                    ) : (
                      <Product key={o._id} product={o} />
                    ),
                  )}
                </div>
              ) : (
                <div key={0} className="product-grid grid-layout columns-4">
                  {(list || []).map((o: IProduct) => (
                    <Product key={o._id} product={o} />
                  ))}
                </div>
              )}
            </div>
          </div>
          {!fiveProduct && hasSmallProduct && (
            <div className="kc-elm kc-css-91 kc_col-of-5 kc_column kc_col-of-5">
              <div className="kc-col-container">
                <div className="widget woocommerce widget_top_rated_products kc-elm kc-css-523358">
                  <h2 className="widgettitle">Mới nhất</h2>
                  <ul className="product_list_widget">
                    {(productListNewest || []).map((o: IProduct) => (
                      <SmallProduct
                        key={o._id}
                        img={o.imageFromUrl[0]?.url}
                        title={o.title}
                        rated={o.rated}
                        price={o.price}
                      />
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default ProductGrid;

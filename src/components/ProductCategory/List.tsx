import { addToCart } from "actions/cart";
import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import formatPrice from "utils/formatPrice";
import { openQuickview } from "../../actions/product";
import { IProduct } from "../../constants/interfaces";

interface Props {
  product: IProduct;
}

const List: React.FC<Props> = ({ product }: Props) => {
  const dispatch = useDispatch();

  const handleOpenQuickview = (product: IProduct) => (): void => {
    openQuickview(dispatch, true, product);
  };

  const handleAddToCart = (
    event: React.FormEvent<EventTarget>,
    product: IProduct,
  ): void => {
    event.preventDefault();
    addToCart(dispatch, product, 1);
  };

  return (
    <div className="post-93 product type-product status-publish has-post-thumbnail product_cat-best-seller product_cat-true-story product_cat-women product_tag-business product_tag-story product_tag-women book_author-yuni-ayunda book_publisher-buku-satu-press first instock featured shipping-taxable purchasable product-type-simple">
      <div className="product__inner">
        <figure className="product__image">
          {product.oldPrice && <span className="onsale">Sale!</span>}
          <a href="#">
            <span
              className="intrinsic-ratio"
              style={{ paddingBottom: "136.9696969697%" }}
            >
              <img
                className="pustaka-lazyload lazy-loaded"
                src={product.imageFromUrl[0]?.url}
                alt=""
              />
            </span>{" "}
          </a>
        </figure>
        <div className="product__detail">
          <h3 className="product__title" title="Blood Repair">
            <Link to={`/product/${product._id}`}>{product.title}</Link>
          </h3>
          <div className="author">
            {" "}
            <a href="#">{product.author}</a>
          </div>
          <div
            className="star-rating"
            role="img"
            aria-label="Rated 4.50 out of 5"
          >
            <span style={{ width: "90%" }}>
              Rated <strong className="rating">{product.rated}</strong> out of 5
            </span>
          </div>
          <div className="product__excerpt">
            <p>{product.description}</p>
          </div>
          <div className="bottom-wrap">
            <div className="product__price">
              {product.oldPrice ? (
                <>
                  <span className="woocommerce-Price-amount amount">
                    <del>{formatPrice(product.oldPrice)}</del>
                    <sup>đ</sup>
                  </span>{" "}
                  {"  "}
                  <ins>
                    <span className="woocommerce-Price-amount amount">
                      {formatPrice(product.price)}
                      <sup>đ</sup>
                    </span>{" "}
                  </ins>
                </>
              ) : (
                <span className="woocommerce-Price-amount amount">
                  {formatPrice(product.price)}
                  <sup>đ</sup>
                </span>
              )}
            </div>
            <a href="#" className="button yith-wcqv-button">
              Xem nhanh
            </a>{" "}
            <div className="product__action">
              <a
                rel="nofollow"
                href="/pustaka/product-category/true-story/?layout=list&add-to-cart=93"
                className="button product_type_simple add_to_cart_button ajax_add_to_cart"
                onClick={(e) => handleAddToCart(e, product)}
              >
                <i className="simple-icon-bag" />
                <i className="dripicons-plus" />
                <span className="button-label">Thêm vào giỏ</span>
              </a>{" "}
              <button
                className="button yith-wcqv-button"
                onClick={handleOpenQuickview(product)}
              >
                <span>Xem nhanh</span>
              </button>{" "}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default List;

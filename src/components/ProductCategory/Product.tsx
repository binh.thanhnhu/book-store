import React from "react";
import { useDispatch } from "react-redux";
import { openQuickview } from "../../actions/product";
import { addToCart } from "../../actions/cart";
import { IProduct } from "../../constants/interfaces";

import "./Product.scss";
import { Link } from "react-router-dom";
import Star from "components/Star/Star";
import formatPrice from "utils/formatPrice";

interface Props {
  product: IProduct;
}

const Product: React.FC<Props> = ({ product }: Props) => {
  const dispatch = useDispatch();

  const handleQuickview = (product: IProduct) => (): void => {
    openQuickview(dispatch, true, product);
  };

  const handleAddToCart = (
    event: React.FormEvent<EventTarget>,
    product: IProduct,
  ): void => {
    event.preventDefault();
    addToCart(dispatch, product, 1);
  };

  return (
    <div
      className={`grid-item product post-${product._id} type-product status-publish has-post-thumbnail product_cat-best-seller product_cat-drama product_tag-art product_tag-bio product_tag-business book_author-g-blakemore-evans book_publisher-boyds-mills-press book_series-series-4 first outofstock sale shipping-taxable purchasable product-type-variable`}
    >
      <div className="product__inner">
        <figure className="product__image">
          {product.oldPrice && <span className="onsale">Sale!</span>}
          <a href="#">
            <span
              className="intrinsic-ratio"
              style={{ paddingBottom: "136.9696969697%" }}
            >
              <img
                className="pustaka-lazyload lazy-loaded"
                src={product.imageFromUrl[0]?.url}
                width={330}
                height={452}
              />
            </span>{" "}
          </a>
          <div className="product__action">
            <a
              rel="nofollow"
              href="#"
              className="button product_type_variable"
              onClick={(e) => handleAddToCart(e, product)}
            >
              <i className="fas fa-shopping-bag" style={{ fontSize: 22 }}></i>
              <i className="fas fa-plus" style={{ fontSize: 16 }}></i>
              <span className="button-label">Select options</span>
            </a>{" "}
            <button
              className="button yith-wcqv-button"
              onClick={handleQuickview(product)}
            >
              <span>Xem nhanh</span>
            </button>{" "}
          </div>
        </figure>
        <div className="product__detail">
          <h3 className="product__title" title="City of Fallen Angels">
            <Link to={`/product/${product._id}`}>{product.title}</Link>
          </h3>
          <div className="author">
            {" "}
            <a href="#">{product.author}</a>
          </div>
          <div className="product__price">
            {product.oldPrice ? (
              <>
                <span className="woocommerce-Price-amount amount">
                  <del>{formatPrice(product.oldPrice)}</del>
                  <sup>đ</sup>
                </span>{" "}
                {"  "}
                <ins>
                  <span className="woocommerce-Price-amount amount">
                    {formatPrice(product.price)}
                    <sup>đ</sup>
                  </span>{" "}
                </ins>
              </>
            ) : (
              <span className="woocommerce-Price-amount amount">
                {formatPrice(product.price)}
                <sup>đ</sup>
              </span>
            )}
          </div>
          <div className="star-rating">
            {product.rated && Star(product.rated || 5)}
            <span style={{ width: "80%" }}>
              Rated <strong className="rating">{product.rated}</strong> out of 5
            </span>
          </div>
          <a
            href="#"
            className="button yith-wcqv-button"
            style={{ position: "relative", zoom: 1 }}
          >
            Xem nhanh
            <div className="blockUI" style={{ display: "none" }} />
            <div
              className="blockUI blockOverlay"
              style={{
                zIndex: 1000,
                border: "none",
                margin: 0,
                padding: 0,
                width: "100%",
                height: "100%",
                top: 0,
                left: 0,
                background:
                  'url("https://demo2.tokomoo.com/pustaka/wp-content/plugins/yith-woocommerce-quick-view/assets/image/qv-loader.gif") center center no-repeat rgb(255, 255, 255)',
                opacity: "0.5",
                cursor: "none",
                position: "absolute",
              }}
            />
            <div
              className="blockUI blockMsg blockElement"
              style={{
                zIndex: 1011,
                display: "none",
                position: "absolute",
                left: 0,
                top: 0,
              }}
            />
          </a>{" "}
        </div>
      </div>
    </div>
  );
};

export default Product;

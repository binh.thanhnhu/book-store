import React, { useEffect, useState } from "react";
import ProductGrid from "./ProductGrid";
import { useHistory, useLocation } from "react-router";
import ProductList from "./ProductList";
import addParams from "../../utils/addParams";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../..";
import getParams from "../../utils/getParams";
import { fetchCategory } from "actions/product";
import classNames from "classnames";
import { Link } from "react-router-dom";
import getMenuName from "utils/getMenuName";
import { BookType } from "constants/data";

interface Props {
  search?: boolean;
}

const ProductCategory: React.FC<Props> = ({ search }: Props) => {
  const [isGrid, setGrid] = useState<boolean>(true);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [sortby, setSortby] = useState<string>("menu_order");
  const [_typeName, setTypeName] = useState<string>("");
  const [_optionName, setOptionName] = useState<string>("");

  const category = useSelector((state: RootState) => state.product.category);
  const message = useSelector(
    (state: RootState) => state.product.message.category,
  );
  const totalPages = useSelector(
    (state: RootState) => state.product.totalPages,
  );

  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  useEffect(() => {
    const { type, option, page, layout, sort, sortName, keyword } = getParams(
      location.search,
    );

    const { typeName, optionName } = getMenuName(BookType, type, option);
    setTypeName(typeName || "Danh sách");
    setOptionName(optionName || "");

    setGrid(layout === "grid" || layout === undefined);
    setCurrentPage(parseInt(page || "1"));
    setSortby(
      JSON.stringify({ sortName, sort: parseInt(sort) }) || "menu_order",
    );

    search
      ? fetchCategory(dispatch, null, { page, sortName, sort, keyword })
      : fetchCategory(dispatch, null, {
          type,
          option,
          page,
          sortName,
          sort,
          keyword,
        });
  }, [location]);

  const handleChangePage = (page: number): void => {
    if (page === 0) return;
    history.push({ search: addParams(location.search, { page }) });
  };

  const handleSortbyChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const { value } = event.currentTarget;
    const { sortName, sort } = JSON.parse(value);
    setSortby(JSON.parse(value));
    history.push({
      search: addParams(location.search, { page: 1, sortName, sort }),
    });
  };

  const arr: number[] = [];
  for (let i = 0; i < totalPages; i++) {
    arr.push(i);
  }

  return (
    <main className="main-content">
      <div className="container">
        <div className="row">
          <div className="content-area ">
            <article className="page type-page">
              <div className="product-sorting">
                <div className="product-layout-view">
                  <Link
                    className={isGrid ? "active" : ""}
                    title="Grid Layout"
                    to={{
                      search: addParams(location.search, {
                        layout: "grid",
                      }),
                    }}
                  >
                    <i className="fas fa-th"></i>
                  </Link>
                  <Link
                    className={!isGrid ? "active" : ""}
                    title="List Layout"
                    to={{
                      search: addParams(location.search, {
                        layout: "list",
                      }),
                    }}
                  >
                    <i className="fas fa-list"></i>
                  </Link>
                </div>
                <div className="woocommerce-notices-wrapper" />
                <form className="woocommerce-ordering" method="get">
                  <select
                    name="sortby"
                    value={sortby}
                    onChange={handleSortbyChange}
                  >
                    <option value="menu_order">Sắp xếp mặc định</option>
                    <option
                      value={JSON.stringify({ sortName: "rated", sort: -1 })}
                    >
                      Điểm rating
                    </option>
                    <option
                      value={JSON.stringify({
                        sortName: "createdAt",
                        sort: -1,
                      })}
                    >
                      Mới nhất
                    </option>
                    <option
                      value={JSON.stringify({
                        sortName: "createdAt",
                        sort: 1,
                      })}
                    >
                      Cũ nhất
                    </option>
                    <option
                      value={JSON.stringify({ sortName: "price", sort: 1 })}
                    >
                      Giá: thấp đến cao
                    </option>
                    <option
                      value={JSON.stringify({ sortName: "price", sort: -1 })}
                    >
                      Giá: cao đến thấp
                    </option>
                  </select>
                  <input type="hidden" name="paged" defaultValue={1} />
                </form>
              </div>
              {message ? (
                <section className="kc-elm kc-css-514909 kc_row kc-pc-loaded">
                  <div className="kc-product-list">
                    <div className={`kc-elm kc-css- kc-title-wrap`}>
                      <h3 className="kc_title">
                        <a href="#" className="kc_title_link">
                          {search
                            ? "Danh sách"
                            : _optionName
                            ? `${_typeName} - ${_optionName}`
                            : _typeName}
                        </a>
                      </h3>
                    </div>
                    <div className="kc-elm kc-css-989445 divider_line">
                      <div className="divider_inner divider_line1" />
                    </div>
                  </div>
                  <h1 className="kc-none-product">{message}</h1>
                </section>
              ) : isGrid ? (
                <ProductGrid
                  id="514909"
                  name={
                    search
                      ? "Danh sách"
                      : _optionName
                      ? `${_typeName} - ${_optionName}`
                      : _typeName
                  }
                  category
                  list={category}
                />
              ) : (
                <ProductList
                  id="601292"
                  name={
                    search
                      ? "Danh sách"
                      : _optionName
                      ? `${_typeName} - ${_optionName}`
                      : _typeName
                  }
                  list={category}
                />
              )}
              <nav className="woocommerce-pagination">
                <ul className="page-numbers">
                  {arr.map((index) => (
                    <li key={index} onClick={() => handleChangePage(index + 1)}>
                      <span
                        aria-current="page"
                        className={classNames("page-numbers", {
                          current: currentPage === index + 1,
                        })}
                      >
                        {index + 1}
                      </span>
                    </li>
                  ))}
                  <li
                    onClick={
                      currentPage < totalPages
                        ? () => handleChangePage(currentPage + 1)
                        : () => handleChangePage(0)
                    }
                  >
                    <span className="next page-numbers">→</span>
                  </li>
                </ul>
              </nav>
            </article>
          </div>
        </div>
      </div>
    </main>
  );
};
export default ProductCategory;

import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { removeProductInCart } from "../../actions/cart";
import { IProduct } from "../../constants/interfaces";
import formatPrice from "../../utils/formatPrice";
import Input from "../Input/Input";

import "./ProductInCart.scss";

interface Props {
  product: IProduct;
  quantity: number | string;
  onTransQuantity: (qty: string) => void;
}

const ProductInCart: React.FC<Props> = ({
  product,
  quantity,
  onTransQuantity,
}: Props) => {
  const [qty, setQty] = useState<number | string>(1);
  const dispatch = useDispatch();

  useEffect(() => {
    setQty(quantity);
  }, [quantity]);

  const handleQuantity = (event: React.FormEvent<HTMLInputElement>): void => {
    const { value } = event.currentTarget;
    (parseInt(value) >= 0 || value === "") && setQty(event.currentTarget.value);
    onTransQuantity(event.currentTarget.value);
  };

  const handleRemoveProduct = (
    event: React.FormEvent<EventTarget>,
    _id: string,
  ) => {
    event.preventDefault();
    removeProductInCart(dispatch, _id);
    onTransQuantity("");
  };

  return (
    <tr className="woocommerce-cart-form__cart-item cart_item">
      <td className="product-remove">
        <a
          href="#"
          className="remove"
          aria-label="Remove this item"
          onClick={(e) => handleRemoveProduct(e, product._id)}
        >
          <i className="fas fa-trash"></i>
        </a>{" "}
      </td>
      <td className="product-thumbnail">
        <a href="#">
          <img
            src={product.imageFromUrl[0]?.url}
            className="attachment-woocommerce_thumbnail size-woocommerce_thumbnail loading"
            alt=""
          />
        </a>{" "}
      </td>
      <td className="product-name" data-title="Product">
        <Link to={`/product/${product._id}`}>{product.title}</Link>{" "}
      </td>
      <td className="product-price" data-title="Price">
        <span className="woocommerce-Price-amount amount">
          {formatPrice(product.price)}
          <sup>đ</sup>
        </span>{" "}
      </td>
      <td className="product-quantity" data-title="Quantity">
        <div className="quantity">
          <label
            className="screen-reader-text"
            htmlFor={`quantity_${product._id}`}
          >
            {`${product.title} quantity`}
          </label>
          <Input
            tag="input"
            type="number"
            id={`quantity_${product._id}`}
            className="input-text qty text"
            value={qty}
            title="Qty"
            inputMode="numeric"
            onChange={handleQuantity}
          />
        </div>
      </td>
      <td className="product-subtotal" data-title="Total">
        <span className="woocommerce-Price-amount amount">
          {formatPrice(product.price * parseInt(qty.toString() || "0"))}
          <sup>đ</sup>
        </span>{" "}
      </td>
    </tr>
  );
};

export default ProductInCart;

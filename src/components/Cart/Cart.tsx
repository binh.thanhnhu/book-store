import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ProductInCart from "./ProductInCart";
import Input from "../Input/Input";
import { RootState } from "../..";
import { IProduct, IProductInCart } from "../../constants/interfaces";
import { setTotalPriceTemp, updateCart } from "../../actions/cart";

import "./Cart.scss";
import totalPriceFunc from "utils/totalPriceFunc";

const Cart: React.FC = () => {
  const dispatch = useDispatch();
  const productInCart = useSelector(
    (state: RootState) => state.cart.productInCart,
  );
  const totalPriceTemp = useSelector(
    (state: RootState) => state.cart.totalPriceTemp,
  );

  const [productInCartState, setProductInCartState] = useState<
    IProductInCart[]
  >(JSON.parse(JSON.stringify(productInCart)));

  const handleChangeQuantity = (product: IProduct, quantity: string): void => {
    let qty = parseInt(quantity || "0");
    if (qty < 0) qty = 0;
    const tempProduct = productInCartState;
    for (let i = 0; i < tempProduct.length; i++) {
      if (tempProduct[i].product._id === product._id) {
        tempProduct[i].quantity = qty;
        break;
      }
    }
    setProductInCartState([...tempProduct]);

    const { total } = totalPriceFunc(productInCartState);
    setTotalPriceTemp(dispatch, total);
  };

  const handleAddToCart = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    updateCart(dispatch, productInCartState, totalPriceTemp);
  };

  return (
    <form className="woocommerce-cart-form" onSubmit={handleAddToCart}>
      <table
        className="shop_table shop_table_responsive cart woocommerce-cart-form__contents"
        cellSpacing={0}
      >
        <thead>
          <tr className="woocommerce-cart-form__cart-item cart_item">
            <th className="product-remove">&nbsp;</th>
            <th className="product-thumbnail">&nbsp;</th>
            <th className="product-name">Sản phẩm</th>
            <th className="product-price">Giá</th>
            <th className="product-quantity">Số lượng</th>
            <th className="product-subtotal">Tổng</th>
          </tr>
        </thead>
        <tbody>
          {productInCart.map((o: IProductInCart) => (
            <ProductInCart
              key={o.product._id}
              product={o.product}
              quantity={o.quantity}
              onTransQuantity={(qty: string) =>
                handleChangeQuantity(o.product, qty)
              }
            />
          ))}
          <tr>
            <td colSpan={6} className="actions">
              <div className="coupon">
                <label htmlFor="coupon_code" className="hidden">
                  Mã Giảm Giá:{" "}
                </label>
                <Input
                  tag="input"
                  type="text"
                  name="coupon_code"
                  className="input-text"
                  id="coupon_code"
                  defaultValue=""
                  placeholder="Mã giảm giá"
                />{" "}
                <Input
                  tag="button"
                  type="button"
                  className="button"
                  name="apply_coupon"
                  value="Apply coupon"
                >
                  Xác Nhận
                </Input>
              </div>
              <Input
                tag="button"
                type="submit"
                className="button"
                name="update_cart"
                value="Update cart"
              >
                Cập Nhật
              </Input>
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  );
};

export default Cart;

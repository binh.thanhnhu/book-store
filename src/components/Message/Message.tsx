import classNames from "classnames";
import React from "react";

interface Props {
  children?: React.ReactNode;
  fullWidth?: boolean;
  error?: boolean;
}

const Message: React.FC<Props> = ({ children, fullWidth, error }: Props) => {
  return fullWidth ? (
    <ul className="wc_payment_methods payment_methods methods">
      <li
        className={classNames(
          "woocommerce-notice woocommerce-notice--info woocommerce-message",
          { "message-error": error },
        )}
      >
        <i className="fas fa-pencil"></i>
        {children}
      </li>{" "}
    </ul>
  ) : (
    <div className="woocommerce-form-coupon-toggle">
      <div
        className={classNames("woocommerce-message", {
          "message-error": error,
        })}
      >
        <i className="fas fa-pencil"></i>
        {children}
      </div>
    </div>
  );
};

export default Message;

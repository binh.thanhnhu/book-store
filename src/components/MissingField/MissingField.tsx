import React from "react";

interface Props {
  errors: any;
}

const MissingField: React.FC<Props> = ({ errors }: Props) => {
  return (
    <div className="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout">
      <ul className="woocommerce-error" role="alert">
        {Object.keys(errors).map((key, i) => (
          <li key={i}>{errors[key]}</li>
        ))}
      </ul>
    </div>
  );
};

export default MissingField;

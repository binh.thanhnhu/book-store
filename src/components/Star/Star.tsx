import React from "react";

const Star = (rated: number): JSX.Element[] => {
  const ratedHTML = [];
  for (let i = 0; i < rated; i++) {
    ratedHTML.push(<i key={i} className="far fa-star active"></i>);
  }
  for (let i = rated; i < 5; i++) {
    ratedHTML.push(
      <i key={i} className="far fa-star" style={{ color: "gray" }}></i>,
    );
  }
  return ratedHTML;
};

export default Star;
